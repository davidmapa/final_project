<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("Edit Donor");
require('prj_header.php');
require('prj_sidebar.php');


# Code for your web page follows.
if (!isset($_POST['submit']))
{  
  
  $donoridin =  $_GET['id'];
  
  // Open donor table at a specific record
  try
  {
  
	  //open the database
	  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
	  $sql="SELECT d.donor_id as 'donor_id', 
			   dt.donortype as 'donortype', 
			   d.firstname as 'firstname',
			   d.lastname as 'lastname', 
			   d.knownbyname as 'knownbyname',
			   d.companydonorname as 'companyname', 
			   d.addressline1 as 'addressline1',
			   d.addressline2 as 'addressline2',
			   d.addressline3 as 'addressline3',
			   d.city as 'city', 
			   d.statecode as 'statecode', 
			   d.zipcode as 'zipcode', 
			   d.phone1 as 'phone1', 
			   d.phone2 as 'phone2', 
			   d.email1 as 'email1',
			   d.email2 as 'email2',
			   d.remarks as 'remarks',
			   a.active as 'active', 
			   d.activedate as 'activedate',
			   (SELECT SUM(contributionamount) FROM donation WHERE donor_id = d.donor_id) as 'sumcontributionamount',
		       (SELECT SUM(inkind_actualvalue) FROM donation WHERE donor_id = d.donor_id) as 'suminkindactualvalue'
			FROM donor as d
			  LEFT JOIN active as a ON d.active_id  = a.active_id
			  LEFT JOIN donortype as dt ON d.donortype_id = dt.donortype_id
			WHERE d.donor_id = $donoridin;";
			
		$result = $db->query($sql);
		foreach($result as $row) {
			$donoridv = $row['donor_id'];
			$donortypev = $row['donortype'];
			$firstnamev = $row['firstname'];
			$lastnamev = $row['lastname'];
			$knownbynamev = $row['knownbyname'];
			$companynamev = $row['companyname'];
			$addressLine1v = $row['addressline1'];
			$addressLine2v = $row['addressline2'];
			$addressLine3v = $row['addressline3'];
			$cityv = $row['city'];
			$statecodev = $row['statecode'];
			$zipcodev = $row['zipcode'];
			$phone1v = $row['phone1'];
			$phone2v = $row['phone2'];
			$email1v = $row['email1'];
			$email2v = $row['email2'];
			$remarksv = $row['remarks'];
			$activev = $row['active'];
			$activedatev = $row['activedate'];
			$sumcontributionamountv = $row['sumcontributionamount'];
			$suminkindactualvaluev = $row['suminkindactualvalue'];
			
		}
		
		// close the database connection
		$db = NULL;
    }

	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
  }
	
?>

  
  <!-- Display a form to capture information -->
  <h2>Edit a donor</h2>
  <form action="prj_donoredit.php" method="post">
    <table cellpadding="10">
      <tr bgcolor="#E7AE66">
        <td width="200" align="center"><b>Field</b></td>
        <td width="400" align="center"><b>Actual Value</b></td>
		<td width="200" align="center"><b>Edit</b></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donor Id</b></td>
		<td colspan="2" align="left"><input style="border:none;font-size: 16px;font-weight: bold" type="number" name="$donorid_v" value="<?php echo $donoridv;?>" readonly></td>
      </tr>
      <tr>
        <td bgcolor="#E7AE66"><b>Donor Type</b></td>
		<td align="left"><b><?php echo $donortypev;?></b><div align="right"><i>Use dropdown to change ---></i></div></td>
		<td align="left">
			<select name="donortypeid_e" >		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the types table
					$result = $db->query('SELECT * FROM donortype WHERE active_id = 1');
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
					foreach($result as $row)
					{
					  print "<option value=".$row['donortype_id'].">".$row['donortype']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>First Name</b></td>
		<td colspan="2" align="left"><input type="text" name="firstname_e" size="40" maxlength="40" value="<?php echo $firstnamev;?>"></td>
        <!-- <td align="left"><input type="text" name="firstname_e" size="20" maxlength="40"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Last Name</b></td>
		<td colspan="2" align="left"><input type="text" name="lastname_e" size="40" maxlength="40" value="<?php echo $lastnamev;?>"></td>		
        <!-- <td align="left"><input type="text" name="lastname_e" placeholder="Last name" size="40" maxlength="40"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Known By Name</b></td>
		<td colspan="2" align="left"><input type="text" name="knownbyname_e" size="40" maxlength="80" value="<?php echo $knownbynamev;?>"></td>
        <!--<td align="left"><input type="text" name="knownbyname_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Organization Name</b></td>
		<td colspan="2" align="left"><input type="text" name="companyname_e" size="40" maxlength="80" value="<?php echo $companynamev;?>"></td>
        <!-- <td align="left"><input type="text" name="companyname_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Address Line 1</b></td>
		<td colspan="2" align="left"><input type="text" name="addressl1_e" size="40" maxlength="80" value="<?php echo $addressLine1v;?>"></td>
        <!--<td align="left"><input type="text" name="addressl1_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Address Line 2</b></td>
		<td colspan="2" align="left"><input type="text" name="addressl2_e" size="40" maxlength="80" value="<?php echo $addressLine2v;?>"></td>
        <!--<td align="left"><input type="text" name="addressl2_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Address Line 3</b></td>
		<td colspan="2" align="left"><input type="text" name="addressl3_e" size="20" maxlength="80" value="<?php echo $addressLine3v;?>"></td>
        <!--<td align="left"><input type="text" name="addressl3_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>City</b></td>
		<td colspan="2" align="left"><input type="text" name="city_e" size="40" maxlength="80" value="<?php echo $cityv;?>"></td>
        <!--<td align="left"><input type="text" name="city_e" size="20" maxlength="80"></td>-->
      </tr>	  
	  <tr>
        <td bgcolor="#E7AE66"><b>State</b></td>
		<td align="left"><b><?php echo $statecodev;?></b><div align="right"><i>Use dropdown to change ---></i></div></td>
		<td align="left">
			<select name="statecode_e">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the states table
					$result = $db->query('SELECT * FROM states ORDER BY statecode');
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
					foreach($result as $row)
					{
					  print "<option value=".$row['statecode'].">".$row['state']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Zip Code</b></td>
		<td colspan="2" align="left"><input type="text" name="zipcode_e" size="12" maxlength="12" value="<?php echo $zipcodev;?>"></td>
        <!--<td align="left"><input type="text" name="zipcode_e" size="12" maxlength="12"></td>-->
      </tr>	
	  <tr>
        <td bgcolor="#E7AE66"><b>Phone 1</b></td>
		<td colspan="2" align="left"><input type="text" name="phone1_e" size="20" maxlength="20" value="<?php echo $phone1v;?>"></td>
        <!--<td align="left"><input type="text" name="phone1_e" size="20" maxlength="20"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Phone 2</b></td>
		<td colspan="2" align="left"><input type="text" name="phone2_e" size="20" maxlength="20" value="<?php echo $phone2v;?>"></td>
        <!--<td align="left"><input type="text" name="phone2_e" size="20" maxlength="20"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Email 1</b></td>
		<td colspan="2" align="left"><input type="text" name="email1_e" size="40" maxlength="80" value="<?php echo $email1v;?>"></td>
        <!--<td align="left"><input type="text" name="email1_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Email 2</b></td>
		<td colspan="2" align="left"><input type="text" name="email2_e" size="40" maxlength="80" value="<?php echo $email2v;?>"></td>
        <!--<td align="left"><input type="text" name="email2_e" size="20" maxlength="80"></td>-->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Remark</b></td>
		<td colspan="2" align="left"><textarea name="remark_e" maxlength="1000" rows="5" cols="50"><?php echo $remarksv;?></textarea></td>        
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Status</b></td>
		<td align="left"><b><?php echo $activev;?></b><div align="right"><i>Use dropdown to change ---></i></div></td>
        <td align="left">
			<select name="statusid_e">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the types table
					$result = $db->query('SELECT * FROM active order by active');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['active_id'].">".$row['active']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
		</td>
      </tr>	  
	  <tr>
        <td bgcolor="#E7AE66"><b>Status Date</b></td>
        <td colspan="2" align="left"><input type="date" name="activedate_e" size="10" maxlength="10" value="<?php echo $activedatev;?>"></td>
      </tr>	
	  <tr>
        <td bgcolor="#E7AE66"><b>Total Amount Donated</b></td>
		<td colspan="2" align="left"><b><?php echo number_format($sumcontributionamountv,2);?></b></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Total Inkind Value Amount Donated</b></td>
		<td colspan="2" align="left"><b><?php echo number_format($suminkindactualvaluev,2);?></b></td>  		
      </tr>
      <tr>
        <td colspan="3" align="center"><input type="submit" name="submit" value="Save Edit"></td>
      </tr>
    </table><br />
  </form>
<?php
} else {
  # Process the information from the form displayed
  $donorid = $_POST['$donorid_v'];
  $donortype = $_POST['donortypeid_e'];
  $companyname = $_POST['companyname_e'];
  $firstname = $_POST['firstname_e'];
  $lastname = $_POST['lastname_e'];
  $knownbyname = $_POST['knownbyname_e'];
  $addressl1 = $_POST['addressl1_e'];
  $addressl2 = $_POST['addressl2_e'];
  $addressl3 = $_POST['addressl3_e'];
  $city = $_POST['city_e'];
  $statecode = $_POST['statecode_e'];
  $zipcode = $_POST['zipcode_e'];
  $phone1 = $_POST['phone1_e'];
  $phone2 = $_POST['phone2_e'];
  $email1 = $_POST['email1_e'];
  $email2 = $_POST['email2_e'];
  $remarks = $_POST['remark_e'];
  $statusid = $_POST['statusid_e'];
  $activedate = $_POST['activedate_e'];
  
  //clean up and validate data
  $donortype = trim($donortype);
  if ( empty($donortype) ) {
    try_again("You must select a donor type. Please select a donor type.");
  }
  
  if ($donortype == 2) {
   $companyname = trim($companyname);
   if ( empty($companyname) ) {
     try_again("Company name field cannot be empty. Please enter a company name for the donor type you selected.");
   }
  }
  
  if ($donortype == 3) {
   $firstname = trim($firstname);
   if ( empty($firstname) ) {
     try_again("First name field cannot be empty. Please enter a first name for the donor type you selected.");
   }
   $lastname = trim($lastname);
   if ( empty($lastname) ) {
     try_again("Last name field cannot be empty. Please enter a last name for the donor type you selected.");
   }   
  }
  
  $statusid = trim($statusid);
  if ( empty($statusid) ) {
    try_again("Status field cannot be empty. Please select a status.");
  }
  
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
	
	//update data...
    $db->exec("UPDATE donor 
	             SET donortype_id = '$donortype', 
				     companydonorname = '$companyname', 
					 firstname = '$firstname', 
					 lastname = '$lastname', 
					 knownbyname = '$knownbyname', 
					 addressline1 = '$addressl1', 
	                 addressline2 = '$addressl2', 
					 addressline3 = '$addressl3', 
					 city = '$city', 
					 statecode = '$statecode',
					 zipcode = '$zipcode', 
					 phone1 = '$phone1',
					 phone2 = '$phone2', 
					 email1 = '$email1',
					 email2 = '$email2',
					 remarks = '$remarks', 
					 active_id = '$statusid',
					 activedate = '$activedate'
				WHERE donor_id = '$donorid';");
	
	//now output the data from the update to a simple html table...
    print "<h2>Donor Updated</h2>";
    print "<table border=1>";
    print "<tr bgcolor=#E7AE66>";
	print "  <td width=300 align=center><b>Field</b></td>";
    print "  <td width=400 align=center><b>Value</b></td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM donor where donor_id = '$donorid'")->fetch(PDO::FETCH_ASSOC);
    print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Donor Id</b></td>";
    print "  <td>".$row['donor_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Donor Type</b></td>";
    print "  <td>".$row['donortype_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Organization Name</b></td>";
    print "  <td>".$row['companydonorname']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>First Name</b></td>";
    print "  <td>".$row['firstname']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Last Name</b></td>";
    print "  <td>".$row['lastname']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Known By Name</b></td>";
    print "  <td>".$row['knownbyname']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Address Line 1</b></td>";
    print "  <td>".$row['addressline1']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Address Line 2</b></td>";
    print "  <td>".$row['addressline2']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Address Line 3</b></td>";
    print "  <td>".$row['addressline3']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>City</b></td>";
    print "  <td>".$row['city']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>State</b></td>";
    print "  <td>".$row['statecode']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Zip Code</b></td>";
    print "  <td>".$row['zipcode']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Phone 1</b></td>";
    print "  <td>".$row['phone1']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Phone 2</b></td>";
    print "  <td>".$row['phone2']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Email 1</b></td>";
    print "  <td>".$row['email1']."</td>";
	print "</tr>";	
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Email 2</b></td>";
    print "  <td>".$row['email2']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Remark</b></td>";
    print "  <td>".$row['remarks']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Status Code</b></td>";
    print "  <td>".$row['active_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Activation Date</b></td>";
    print "  <td>".$row['activedate']."</td>";
	print "</tr>";	
    print "</table><br/>";
	
    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
 
}
require('prj_footer.php');
?>
