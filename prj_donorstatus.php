<!doctype html>


<?php
require('prj_functions.php');
require('prj_values.php');
html_head("prj donor status");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{

	# Code for your web page follows.
	try
	{
	   
	  //open the database
	  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
	  $sql="SELECT CONCAT(YEAR(CURDATE()),'-01-01') AS 'begindate', CURDATE() AS 'enddate';";
	  
	  $result = $db->query($sql);
	  foreach($result as $row) {
		$begindate = $row['begindate'];
		$enddate = $row['enddate'];
	  }
	  
	  // close the database connection
	  $db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
    }
?>

	<h2>Donor Status Date Range</h2>
	<form action="prj_donorstatus.php" method="post">
		<table border="0" cellpadding=10>
		  <tr>
		    <td align="left" colspan="2">Enter the date range to retrieve the donor listings within the range.<br /> 
			                             The default begin date is January 1 of the current year and <br /> the end date is the current date. The default dates can be changed.</td>		
		  </tr>
		  <tr bgcolor="#E7AE66">
			<td width="200" align="center"><b>Begin Date</b></td>
			<td width="200" align="center"><b>End Date</b></td>			
		  </tr>
		  <tr>
			<td align="center"><input type="date" name="begindate" size="10" maxlength="10" value="<?php echo $begindate;?>"></td>
			<td align="center"><input type="date" name="enddate" size="10" maxlength="10" value="<?php echo $enddate;?>"></td>			
		  </tr>	  
		  <tr>
			<td colspan="2" align="center"><input type="submit" name="submit" value="Retrieve Records"></td>
		  </tr>
		</table>
	</form><br />	

<?php
} else {
	  # Process the information from the form displayed
	  $begindate = $_POST['begindate'];
	  $enddate = $_POST['enddate']; 

	  // Build Report Date line
	  $reportdates = "<b>Donor Status period:</b> ".$begindate." <b>through</b> ".$enddate;	  
?>
  
	<h2>Donor Status</h2>
	<!-- display all equipment -->
	<table border="1" cellpadding="10">
<?php	
	print "<tr>";
    print "  <td colspan=11 align=center bgcolor=#E7AE66>".$reportdates."</td>"; 
    print "</tr>";
?>
	  <tr>
		<td align="left" colspan="11">A <b>donor</b> in general is a person, organization or government who voluntarily donates something of value such as money, 
		professional services, gifts, etc. to a charity (non-profit) organization.  In this application <b>WAMMP</b> is the designated <b>donee</b>, the receiver of the donated gift. </td>		
	  </tr>	  
	  <tr bgcolor="#E7AE66">
		<td align="center"><b>Donor Id</b></td>
		<td align="center"><b>Donor Type</b></td>
		<td align="center"><b>Donor Name</b></td>
		<td align="center"><b>Phone</b></td>
		<td align="center"><b>Email</b></td>
		<td align="center"><b>Status</b></td>
		<td align="center"><b>Date</b></td>
		<td align="center"><b>Total Donated</b></td>
		<td align="center"><b>Inkind Value</b></td>
		<td align="center"><b>Edit</b></td>
		<td align="center"><b>View</b></td>	
	  </tr>

<?php

	try
	{
	  
	  //open the database
	  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
	  $sql="SELECT d.donor_id as 'donor_id', 
			   dt.donortype as 'donortype',
               CASE 
			    WHEN LENGTH(TRIM(companydonorname)) = 0 THEN CONCAT(firstname,' ',lastname)
				ELSE TRIM(companydonorname)
			   END AS 'donorname',			   
			   d.city as 'city', 
			   d.statecode as 'state', 
			   d.phone1 as 'phone', 
			   d.email1 as 'email', 
			   a.active as 'active', 
			   d.activedate as 'activedate',
			   (SELECT CASE 
            		    WHEN SUM(contributionamount) >= 0.0 THEN SUM(contributionamount)
					    ELSE 0.0 
					   END 
				FROM donation WHERE donor_id = d.donor_id) as 'sumcontributionamount',
			   (SELECT CASE 
            		    WHEN SUM(inkind_actualvalue) >= 0.0 THEN SUM(inkind_actualvalue)
						ELSE 0.0 
					   END
				FROM donation WHERE donor_id = d.donor_id) as 'suminkindactualvalue'
			FROM donor as d
			  LEFT JOIN active as a ON d.active_id  = a.active_id
			  LEFT JOIN donortype as dt ON d.donortype_id = dt.donortype_id
			WHERE d.activedate BETWEEN '$begindate' AND '$enddate'
			ORDER BY activedate;";
	  
	  	  
	  $result = $db->query($sql);
	  foreach($result as $row) {
		print "<tr>";
		print "  <td><b>".$row['donor_id']."</b></td>";
		print "  <td>".$row['donortype']."</td>";
		print "  <td>".$row['donorname']."</td>";
		print "  <td>".$row['phone']."</td>";
		print "  <td>".$row['email']."</td>";
		print "  <td>".$row['active']."</td>";
		print "  <td>".$row['activedate']."</td>";
		print "  <td align=right>".number_format($row['sumcontributionamount'],2)."</td>";
		print "  <td align=right>".number_format($row['suminkindactualvalue'],2)."</td>";
		print "  <td><a href='prj_donoredit.php?id=".$row['donor_id']."'>click to edit</a></td>";
		print "  <td><a href='prj_donorview.php?id=".$row['donor_id']."'>click to view</a></td>";
		print "</tr>";
	  }

	  print "</table><br />";

	  // close the database connection
	  $db = NULL;
	}
	catch(PDOException $e)
	{
	  echo 'Exception : '.$e->getMessage();
	  echo "<br/>";
	  $db = NULL;
	}
}
require('prj_footer.php');
?>
