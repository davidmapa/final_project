<!doctype html>
<?php

require('prj_functions.php');
require('prj_values.php');
html_head("Add lookup");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
	
	$luaddtype =  $_GET['id'];
		
	if ( $luaddtype == 1 ) {
	
		$addanew = "Add a new State record";
				
	} elseif ( $luaddtype == 2 ) {
	
		$addanew = "Add a new Category record";
		
	} elseif ( $luaddtype == 3 ) {
	
		$addanew = "Add a new Sub-Category record";
		
	} elseif ( $luaddtype == 4 ) {
	
		$addanew = "Add a new Contribution Source record";
		
	} else {

		$addanew = "Add a new Donor Type record";
		
    }

	echo "<h2>" . $addanew . "</h6>"; 
	
?>

  <!-- Display a form to capture information -->
  
  <form action="prj_lookupadd.php" method="post">
    <table id="add" border="0" cellpadding="10">
      <tr bgcolor="#E7AE66">
        <td width="300" align="center"><b>Field</b></td>
        <td width="400" align="center"><b>Value</b></td>
      </tr>
      <tr>
        <td bgcolor="#E7AE66"><b>Entry Identifier</b></td>
        <td align="left"><input type="text" name="entryid" size="50" maxlength="100"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Description</b></td>
        <td align="left"><input type="text" name="description" size="50" maxlength="100"></td>
      </tr>
	  <tr>
        <td colspan="2" align="center"><input type="number" name="luaddtype" value="<?php echo $luaddtype;?>" hidden ></td>
      </tr>
	  <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table><br />
  </form>
<?php
		
} else {
  # Process the information from the form displayed
  $entryid = $_POST['entryid'];
  $description = $_POST['description'];
  $luaddtype =  $_POST['luaddtype'];
  
  //clean up and validate data
  $entryid = trim($entryid);
  if ( empty($entryid) ) {
    try_again("Entry Identifier field cannot be empty. Please enter an entry identifier.");
  }
  
  $description = trim($description);
  if ( empty($description) ) {
    try_again("Description field cannot be empty. Please enter a description.");
  }
  
  
  
  if ( $luaddtype == 1 ) {
	  
	//Check length
	if ( strlen($entryid) <> 2 ) {
	  try_again("State code field must be two characters. Please enter a two character state code.");
	}  
	  
	// Count the statecode entered in the states table to check for duplications
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM states 
							  WHERE UPPER(statecode) = UPPER('$entryid');");
			
		foreach($result as $row)
		{
		  $rowcont = $row['rowcnt'];
		}

		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}
	
	//Check for duplicate
	if ( $rowcont > 0 ) {
	  try_again("Statecode entered is a duplicate entry. The Statecode must be unique.");
	}		  
	
	try
	  {		
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//insert data...
		$db->exec("INSERT INTO states (statecode, state ) 
				   VALUES ('$entryid', '$description' );");
	  
		//get the last id value inserted into the table
		//$last_id = $db->lastInsertId();
		
		//now output the data from the insert to a simple html table...
		print "<h2>State Added</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM states where statecode = '$entryid' ")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>State Code</b></td>";
		print "  <td>".$row['statecode']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>State</b></td>";
		print "  <td>".$row['state']."</td>";
		print "</tr>";
		print "</table><br />";
	
		// close the database connection
		$db = NULL;
	}
	  catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}		
		
  } elseif ( $luaddtype == 2 ) {  // End - Add State and Begin - Add Category
	  
	// Count the category entered in the category table to check for duplications
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM donationcategories 
							  WHERE UPPER(donationcategories) = UPPER('$entryid');");
			
		foreach($result as $row)
		{
		  $rowcont = $row['rowcnt'];
		}

		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}
	
	//Check for duplicate
	if ( $rowcont > 0 ) {
	  try_again("Category entered is a duplicate entry. The category must be unique.");
	}		  
	  
	
	try
	  {
		  
		  
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//insert data...
		$db->exec("INSERT INTO donationcategories (donationcategories, description, active_id, activedate ) 
				   VALUES ('$entryid', '$description','1', CURDATE() );");
	  
		//get the last id value inserted into the table
		$last_id = $db->lastInsertId();
		
		//now output the data from the insert to a simple html table...
		print "<h2>Donation Category Added</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM donationcategories where donationcategories_id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Category</b></td>";
		print "  <td>".$row['donationcategories']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Description</b></td>";
		print "  <td>".$row['description']."</td>";
		print "</tr>";
		print "</table><br />";
	
		// close the database connection
		$db = NULL;
	}
	  catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}		
		
  } elseif ( $luaddtype == 3 ) {  // End - Add Category and Begin - Add Restricted Sub-Category
  
 	// Count the restricted sub-category entered in the donation restricted subcat table to check for duplications
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM donationrestrictedsubcat 
							  WHERE UPPER(donationrestrictedsubcat) = UPPER('$entryid');");
			
		foreach($result as $row)
		{
		  $rowcont = $row['rowcnt'];
		}

		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}
	
	//Check for duplicate
	if ( $rowcont > 0 ) {
	  try_again("Restricted sub-category entered is a duplicate entry. The restricted sub-category must be unique.");
	} 
	
	try
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//insert data...
		$db->exec("INSERT INTO donationrestrictedsubcat (donationrestrictedsubcat, description, active_id, activedate ) 
				   VALUES ('$entryid', '$description','1', CURDATE() );");
	  
		//get the last id value inserted into the table
		$last_id = $db->lastInsertId();
		
		//now output the data from the insert to a simple html table...
		print "<h2>Donation Restricted Sub-Category Added</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM donationrestrictedsubcat where donationrestrictedsubcat_id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Restricted Sub-Category</b></td>";
		print "  <td>".$row['donationrestrictedsubcat']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Description</b></td>";
		print "  <td>".$row['description']."</td>";
		print "</tr>";
		print "</table><br />";
	
		// close the database connection
		$db = NULL;
	}
	  catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}		
		
  } elseif ( $luaddtype == 4 ) {  // End - Add Restricted Sub-Category and Begin - Add Contribution source
  
	// Count the contribution source entered in the contribution source table to check for duplications
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM contributionsource 
							  WHERE UPPER(contributionsource) = UPPER('$entryid');");
			
		foreach($result as $row)
		{
		  $rowcont = $row['rowcnt'];
		}

		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}
	
	//Check for duplicate
	if ( $rowcont > 0 ) {
	  try_again("Contribution source entered is a duplicate entry. The contribution source must be unique.");
	}  
	
	try
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//insert data...
		$db->exec("INSERT INTO contributionsource (contributionsource, description, active_id, activedate ) 
				   VALUES ('$entryid', '$description','1', CURDATE() );");
	  
		//get the last id value inserted into the table
		$last_id = $db->lastInsertId();
		
		//now output the data from the insert to a simple html table...
		print "<h2>Donation Contribution Source Added</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM contributionsource where contributionsource_id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Contribution Source</b></td>";
		print "  <td>".$row['contributionsource']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Description</b></td>";
		print "  <td>".$row['description']."</td>";
		print "</tr>";
		print "</table><br />";
	
		// close the database connection
		$db = NULL;
	}
	  catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}		
		
  } else {  // End - Add Contribution source and Begin - Add Donor Type
  
 	// Count the contribution source entered in the contribution source table to check for duplications
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM donortype 
							  WHERE UPPER(donortype) = UPPER('$entryid');");
			
		foreach($result as $row)
		{
		  $rowcont = $row['rowcnt'];
		}

		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}
	
	//Check for duplicate
	if ( $rowcont > 0 ) {
	  try_again("Donor type entered is a duplicate entry. The donor type must be unique.");
	}    

	try
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//insert data...
		$db->exec("INSERT INTO donortype (donortype, description, active_id, activedate ) 
				   VALUES ('$entryid', '$description','1', CURDATE() );");
	  
		//get the last id value inserted into the table
		$last_id = $db->lastInsertId();
		
		//now output the data from the insert to a simple html table...
		print "<h2>Donor Type Added</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM donortype where donortype_id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donor Type</b></td>";
		print "  <td>".$row['donortype']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Description</b></td>";
		print "  <td>".$row['description']."</td>";
		print "</tr>";
		print "</table><br />";
	
		// close the database connection
		$db = NULL;
	}
	  catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}	
		
  }  //End - Add Donor Type
 
}
require('prj_footer.php');
?>
