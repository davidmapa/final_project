<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("Add Donor");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add a donor</h2>
  
  <form action="prj_donor.php" method="post">
    <table border="0" cellpadding="10">	
      <tr>
		<td align="left" colspan="11"><p> A <b>donor</b> in general is a person, organization or government who voluntarily donates something of value such as money, 
		                              professional services, gifts, etc. to a charity (non-profit) organization.  In this application <b>WAMMP</b> is the designed <b>donee</b>, the receiver of the donated gift.</p> </td>		
	  </tr>
	  <tr bgcolor="#E7AE66">
        <td width="200" align="center"><b>Field</b></td>
        <td width="300" align="center"><b>Value</b></td>
      </tr>	  
      <tr>
        <td bgcolor="#E7AE66"><b>Donor Type</b></td>
		<td align="left">
			<select name="donortype">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the types table
					$result = $db->query('SELECT * FROM donortype WHERE active_id = 1 AND donortype_id > 1 ');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['donortype_id'].">".$row['donortype']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
			&nbsp;&nbsp;You can leave the <b>Organization Name</b> field blank if <b>Individual</b> is selected.
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Organization Name</b></td>
        <td align="left"><input type="text" name="companyname" placeholder="Organization name or leave blank" size="50" maxlength="80">&nbsp;&nbsp;Enter the <b>organization name</b> for <b>non-individual</b> selections above.</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>First Name</b></td>
        <td align="left"><input type="text" name="firstname" placeholder="First name" size="40" maxlength="40"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Last Name</b></td>
        <td align="left"><input type="text" name="lastname" placeholder="Last name" size="40" maxlength="40"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Known By Name</b></td>
        <td align="left"><input type="text" name="knownbyname" placeholder="Nick name" size="50" maxlength="80">&nbsp;&nbsp;Optional</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Address Line 1</b></td>
        <td align="left"><input type="text" name="addressl1" placeholder="Street Address or P.O. Box number" size="50" maxlength="80"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Address Line 2</b></td>
        <td align="left"><input type="text" name="addressl2" size="50" maxlength="80"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Address Line 3</b></td>
        <td align="left"><input type="text" name="addressl3" size="50" maxlength="80"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>City</b></td>
        <td align="left"><input type="text" name="city" size="50" maxlength="80"></td>
      </tr>	  
	  <tr>
        <td bgcolor="#E7AE66"><b>State</b></td>
		<td align="left">
			<select name="statecode">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the states table
					$result = $db->query('SELECT * FROM states ORDER BY statecode');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['statecode'].">".$row['state']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Zip Code</b></td>
        <td align="left"><input type="text" name="zipcode" placeholder="Zip code" size="12" maxlength="12"></td>
      </tr>	
	  <tr>
        <td bgcolor="#E7AE66"><b>Phone 1</b></td>
        <td align="left"><input type="text" name="phone1" placeholder="nnn-nnn-nnnn" size="20" maxlength="20">&nbsp;&nbsp;Should include if available</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Phone 2</b></td>
        <td align="left"><input type="text" name="phone2" placeholder="nnn-nnn-nnnn" size="20" maxlength="20">&nbsp;&nbsp;Optional</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Email 1</b></td>
        <td align="left"><input type="text" name="email1" size="50" maxlength="80">&nbsp;&nbsp;Should include if available</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Email 2</b></td>
        <td align="left"><input type="text" name="email2" size="50" maxlength="80">&nbsp;&nbsp;Optional</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Remark</b></td>
		<td align="left"><textarea name="remark" placeholder="Enter remark up to 1000 characters" maxlength="1000" rows="5" cols="80"></textarea></td>
        <!-- <td align="left"><input type="textarea" name="remark" maxlength="1000" cols="80" rows="5"></td> -->
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Status</b></td>
        <td align="left">
			<select name="statusid">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the types table
					$result = $db->query('SELECT * FROM active order by active');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['active_id'].">".$row['active']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
		</td>
      </tr>	  
	  <tr>
        <td bgcolor="#E7AE66"><b>Date (yyyy-mm-dd)</b></td>
        <td align="left"><input type="date" name="activedate" size="10" maxlength="10"></td>
      </tr>	  
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="Add Donor"></td>
      </tr>
    </table><br />
  </form>
<?php
} else {
	  # Process the information from the form displayed
	  $donortype = $_POST['donortype'];
	  $companyname = $_POST['companyname'];
	  $firstname = $_POST['firstname'];
	  $lastname = $_POST['lastname'];
	  $knownbyname = $_POST['knownbyname'];
	  $addressl1 = $_POST['addressl1'];
	  $addressl2 = $_POST['addressl2'];
	  $addressl3 = $_POST['addressl3'];
	  $city = $_POST['city'];
	  $statecode = $_POST['statecode'];
	  $zipcode = $_POST['zipcode'];
	  $phone1 = $_POST['phone1'];
	  $phone2 = $_POST['phone2'];
	  $email1 = $_POST['email1'];
	  $email2 = $_POST['email2'];
	  $remark = $_POST['remark'];
	  $statusid = $_POST['statusid'];
	  $activedate = $_POST['activedate'];
	  
	  //clean up and validate data
	  $donortype = trim($donortype);
	  if ( empty($donortype) ) {
		try_again("You must select a donor type. Please select a donor type.");
	  }
	  
	  if ($donortype == 2) {
	   $companyname = trim($companyname);
	   if ( empty($companyname) ) {
		 try_again("Company name field cannot be empty. Please enter a company name for the donor type you selected.");
	   }
	  }
	  
	  if ($donortype == 3) {
	   $firstname = trim($firstname);
	   if ( empty($firstname) ) {
		 try_again("First name field cannot be empty. Please enter a first name for the donor type you selected.");
	   }
	   $lastname = trim($lastname);
	   if ( empty($lastname) ) {
		 try_again("Last name field cannot be empty. Please enter a last name for the donor type you selected.");
	   }   
	  }
	  
	  $zipcode = trim($zipcode);
	  if(preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$zipcode)){
         $zipcode = trim($zipcode);
      } else {
         try_again("You did not enter a valid zipcode. Please entere a valid zipcode of form nnnnn or nnnnn-nnnn.");
      }

      $phone1 = trim($phone1);
	  if(preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i",$phone1)){
         $phone1 = trim($phone1);
      } else {
         try_again("You did not enter a valid phone number. Please enter a valid phone number.");
      } 
	  
	  $phone2 = trim($phone2);  // Optional phone
	  if(strlen($phone2) > 0){
		if(preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i",$phone2)){
         $phone2 = trim($phone2);
		} else {
			 try_again("You did not enter a valid phone number. Please enter a valid phone number.");
		}  		  
	  }	  
	  
	  $statusid = trim($statusid);
	  if ( empty($statusid) ) {
		try_again("Status field cannot be empty. Please select a status.");
	  }
	  
	  try  // Check for duplicate company or organization
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM donor 
							  WHERE trim(companydonorname) = trim('$companyname')
							    AND length(companydonorname) > 0
								AND donortype_id != 3;");
				
		foreach($result as $row)
		{
		  $rowcount = $row['rowcnt'];
		}
		
		// close the database connection
		$db = NULL;
	  }
	  catch(PDOException $e)
	  {
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	  }
		
	  if ( $rowcount > 0 ) {
		try_again($companyname." is not unique. Company names must be unique.");
	  }
	  
	  try  // Check for duplicate individuals
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		//Get count
		$result = $db->query("SELECT COUNT(*) AS rowcnt 
							  FROM donor 
							  WHERE trim(firstname) = trim('$firstname')
								AND trim(lastname) = trim('$lastname')
								AND donortype_id = 3;");
				
		foreach($result as $row)
		{
		  $rowcount = $row['rowcnt'];
		}
		
		// close the database connection
		$db = NULL;
	  }
	  catch(PDOException $e)
	  {
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	  }
		
	  if ( $rowcount > 0 ) {
		try_again($firstname." ".$lastname." is not unique. Individual names entered must be unique.");
	  }
	  
	  try  // Write the new donor record
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  	
		//insert data...
		$db->exec("INSERT INTO donor (donortype_id, companydonorname, firstname, lastname, knownbyname, addressline1, 
									  addressline2, addressline3, city, statecode, zipcode, phone1, phone2, email1, 
									  email2, remarks, active_id, activedate ) 
							VALUES ('$donortype', '$companyname', '$firstname', '$lastname', '$knownbyname', '$addressl1', 
									'$addressl2', '$addressl3', '$city', '$statecode', '$zipcode', '$phone1', '$phone2', '$email1',
									'$email2', '$remarks', '$statusid', '$activedate' );");
	  
		//get the last id value inserted into the table
		$last_id = $db->lastInsertId();
		
		//now output the data from the insert to a simple html table...
		print "<h2>Donor Added</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM donor where donor_id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donor Id</b></td>";
		print "  <td>".$row['donor_id']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donor Type</b></td>";
		print "  <td>".$row['donortype_id']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Company Name</b></td>";
		print "  <td>".$row['companyname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>First Name</b></td>";
		print "  <td>".$row['firstname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Last Name</b></td>";
		print "  <td>".$row['lastname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Known By Name</b></td>";
		print "  <td>".$row['knownbyname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Address Line 1</b></td>";
		print "  <td>".$row['addressl1']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Address Line 2</b></td>";
		print "  <td>".$row['addressl2']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Address Line 3</b></td>";
		print "  <td>".$row['addressl3']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>City</b></td>";
		print "  <td>".$row['city']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>State</b></td>";
		print "  <td>".$row['statecode']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Zip Code</b></td>";
		print "  <td>".$row['zipcode']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Phone 1</b></td>";
		print "  <td>".$row['phone1']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Phone 2</b></td>";
		print "  <td>".$row['phone2']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Email 1</b></td>";
		print "  <td>".$row['email1']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Email 2</b></td>";
		print "  <td>".$row['email2']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Remark</b></td>";
		print "  <td>".$row['remark']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Status Code</b></td>";
		print "  <td>".$row['statusid']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Activation Date</b></td>";
		print "  <td>".$row['activedate']."</td>";
		print "</tr>";	
		print "</table>";
		
		// close the database connection
		$db = NULL;
	  }
	  catch(PDOException $e)
	  {
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	  }
 
}
require('prj_footer.php');
?>
