<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("Edit Donation");
require('prj_header.php');
require('prj_sidebar.php');


# Code for your web page follows.
if (!isset($_POST['submit']))
{  
  
  $donationin =  $_GET['id'];
  
  // Open donor table at a specific record
  try
  {
  
	  //open the database
	  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
	  $sql="SELECT dn.donation_id as 'donation_id', 
	           trim(CONCAT(dr.firstname, ' ', dr.lastname)) as 'donorname',
			   dr.companydonorname as 'companyname',
			   dnc.donationcategories as 'donationcategories',
			   dnrsc.donationrestrictedsubcat as 'donationrestictedsubcat',
			   dn.donationdate as 'donationdate',
			   cs.contributionsource as 'contributionsource',
			   dn.contributionother as 'contributionother',
			   dn.sourceidentifier as 'sourceidentifier',
			   dn.contributionamount as 'contributionamount',
			   dn.inkind_actualvalue as 'inkind_actualvalue',
			   dn.description
			FROM donation as dn
			  LEFT JOIN donor as dr ON dn.donor_id = dr.donor_id
			  LEFT JOIN donationcategories as dnc ON dn.donationcategories_id = dnc.donationcategories_id
			  LEFT JOIN donationrestrictedsubcat as dnrsc ON dn.donationrestrictedsubcat_id = dnrsc.donationrestrictedsubcat_id
			  LEFT JOIN contributionsource as cs ON dn.contributionsource_id  = cs.contributionsource_id			  
			WHERE  dn.donation_id = $donationin;";
			
		$result = $db->query($sql);
		foreach($result as $row) {
			$donationidv = $row['donation_id'];
			$donornamev = $row['donorname'];
			$companynamev = $row['companyname'];
			$donationcategoriesv = $row['donationcategories'];
			$donationrestictedsubcatv = $row['donationrestictedsubcat'];
			$donationdatev = $row['donationdate'];
			$contributionsourcev = $row['contributionsource'];
			$contributionotherv = $row['contributionother'];
			$sourceidentifierv = $row['sourceidentifier'];
			$contributionamountv = $row['contributionamount'];
			$inkind_actualvaluev = $row['inkind_actualvalue'];
			$descriptionv = $row['description'];			
		}
		
		// close the database connection
		$db = NULL;
    }

	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
  }
	
?>
  
  <!-- Display a form to capture information -->
  <h2>Edit a donors donation</h2>
  <form action="prj_donationedit.php" method="post">
    <table border="0" cellpadding="10">
      <tr bgcolor="#E7AE66">
        <td width="200" align="center"><b>Field</b></td>
        <td width="400" align="center"><b>Actual Value</b></td>
		<td width="200" align="center"><b>Edit</b></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donation Id</b></td>
		<td colspan="2" align="left"><input  style="border:none;font-size: 16px;font-weight: bold" type="number" name="$donationid_v" value="<?php echo $donationidv;?>" readonly></td>		
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donors Name</b></td>
		<td colspan="2" align="left"><b><?php echo $donornamev;?></b></td>	
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donors Organization Name</b></td>
		<td colspan="2" align="left"><b><?php echo $companynamev;?></b></td>
      </tr>
      <tr>
        <td bgcolor="#E7AE66"><b>Donation Category</b></td>
		<td align="left"><b><?php echo $donationcategoriesv;?></b><div align="right"><i>Use dropdown to change ---></i></div></td>
		<td align="left">
			<select name="donationcategories_e">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the donationcategories table
					$result = $db->query('SELECT * FROM donationcategories WHERE active_id = 1');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['donationcategories_id'].">".$row['donationcategories']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
			&nbsp;&nbsp;<a href="prj_catinfo.html" onclick="return popitup('prj_catinfo.html')">Link to explanation</a>
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donation Restricted Sub-Category</b></td>
		<td align="left"><b><?php echo $donationrestictedsubcatv;?></b><div align="right"><i>Use dropdown to change ---></i></div></td>
		<td align="left">
			<select name="donationrestictedsubcat_e">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the donationrestrictedsubcat table
					$result = $db->query('SELECT * FROM donationrestrictedsubcat WHERE active_id = 1');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['donationrestrictedsubcat_id'].">".$row['donationrestrictedsubcat']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
			&nbsp;&nbsp;A Sub-Category is associated to restricted funds
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donation Date</b></td>
		<td colspan="2" align="left"><input type="date" name="donationdate_e" value="<?php echo $donationdatev;?>"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Source</b></td>
		<td align="left"><b><?php echo $contributionsourcev;?></b><div align="right"><i>Use dropdown to change ---></i></div></td>
		<td align="left">
			<select name="contributionsource_e">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the contributionsource table
					$result = $db->query('SELECT * FROM contributionsource WHERE active_id = 1');
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['contributionsource_id'].">".$row['contributionsource']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>			
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Other</b></td>
		<td colspan="2" align="left"><input type="text" name="contributionother_e" size="50" maxlength="80" value="<?php echo $contributionotherv;?>">
		&nbsp;&nbsp;If you select <b>other</b> in <b>Contribution Source</b> above, please explain</td>		
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Source Identifier</b></td>
		<td colspan="2" align="left"><input type="text" name="sourceidentifier_e" size="50" maxlength="80" value="<?php echo $sourceidentifierv;?>">
		&nbsp;&nbsp;Include Check Number, last 4 of Credit Card, etc.</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Amount</b></td>
		<td colspan="2" align="left"><input type="number" name="contributionamount_e" size="10" maxlength="10" value="<?php echo $contributionamountv;?>"></td>        
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Inkind Value Amount</b></td>
		<td colspan="2" align="left"><input type="number" name="inkind_actualvalue_e" size="10" maxlength="10" value="<?php echo $inkind_actualvaluev;?>">
		&nbsp;&nbsp;Gift actual value amount</td>        
      </tr>	  
	  <tr>
        <td bgcolor="#E7AE66"><b>Description</b></td>
		<td colspan="2" align="left"><textarea name="description_e" maxlength="1000" rows="5" cols="80"><?php echo $descriptionv;?></textarea></td>        
      </tr>
      <tr>
		<td align="center" bgcolor="#E7AE66"><b>Record Process Type</b></td>
		<td  colspan="2" align="left"><input type="radio" name="group1" value="deleterow">Delete<br />
		               	 <input type="radio" name="group1" value="updaterow" checked>Update</td><br />
	  </tr>	  
      <tr>
        <td colspan="3" align="center"><input type="submit" name="submit" value="Process Record"></td>
      </tr>
    </table><br />
  </form>
<?php
} else {
  $process = $_POST['group1']; 
  
  if ($process=='deleterow' ) {  // Begin - Delete a selected donation
	  
	$donationid = $_POST['$donationid_v'];
	try
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		//update data...
		$db->exec("DELETE FROM donation WHERE donation_id = '$donationid';");
	 
	    print "<h2>Donation Deleted</h2>";
		print "<table border=0 cellpadding=10>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Id</b></td>";
		print "  <td>". $donationid ."</td>";
		print "</tr>";
		print "</table><br/>";
		
		// close the database connection
		$db = NULL;
	  }
	  catch(PDOException $e)
	  {
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	  }	  
  // End - Delete a selected donation  
  } else {	// Begin - Update a selected donation  
	
	  # Process the information from the form displayed
	  $donationid = $_POST['$donationid_v'];
	  $donationcategories = $_POST['donationcategories_e'];
	  $donationrestictedsubcat = $_POST['donationrestictedsubcat_e'];
	  $donationdate = $_POST['donationdate_e'];
	  $contributionsource = $_POST['contributionsource_e'];
	  $contributionother = $_POST['contributionother_e'];
	  $sourceidentifier = $_POST['sourceidentifier_e'];
	  $contributionamount = $_POST['contributionamount_e'];
	  $inkindactualvalue = $_POST['inkind_actualvalue_e'];
	  $description = $_POST['description_e'];
		
	  //clean up and validate data
	  $donortype = trim($donationcategories);
	  if ( empty($donationcategories) ) {
		try_again("You must select a donation category. Please select a donation category.");
	  }
	  
	  if ($donationcategories == 2) {  // For restricted accounts
	   $donationrestictedsubcat = trim($donationrestictedsubcat);
	   if ( empty($donationrestictedsubcat) ) {
		 try_again("Donation restricted sub-category cannot be empty for the category you selected. Please enter a donation restricted sub-category for the category type you selected.");
	   }
	  }
		
	  $donationdate = trim($donationdate);
	  if ( empty($donationdate) ) {
		try_again("Status date field cannot be empty. Please select a status date.");
	  }
	  
	  $contributionsource = trim($contributionsource);
	  if ( empty($contributionsource) ) {
		try_again("You must select a contribution source. Please select a contribution source.");
	  }
	  
	  if ($donationcategories <> 3) {  // For all accounts except gifts
	   $contributionamount = trim($contributionamount);
	   if ( empty($contributionamount) ) {
		 try_again("You must enter a contribution amount for the selected category. Please enter a contribution amount for the category type you selected.");
	   }
	  }
	  
	  if ($donationcategories == 3) {  // For restricted accounts
	   $inkindactualvalue = trim($inkindactualvalue);
	   if ( empty($inkindactualvalue) ) {
		 try_again("You must enter an in kind value amount for the selected category. Please enter an in kind value amount for the category type you selected.");
	   }
	  }
	  
	  $description = trim($description);
	  if ( empty($description) ) {
		try_again("You must enter a description for this contribution. Please enter a description for this contribution.");
	  }
	  
	  try
	  {
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		//update data...
		$db->exec("UPDATE donation 
					 SET donationcategories_id = '$donationcategories', 
						 donationrestrictedsubcat_id = '$donationrestictedsubcat', 
						 donationdate = '$donationdate', 
						 contributionsource_id = '$contributionsource', 
						 contributionother = '$contributionother', 
						 sourceidentifier = '$sourceidentifier', 
						 contributionamount = '$contributionamount', 
						 inkind_actualvalue = '$inkindactualvalue', 
						 description = '$description'
					WHERE donation_id = '$donationid';");
		
		//now output the data from the update to a simple html table...
		print "<h2>Donation Updated</h2>";
		print "<table border=1>";
		print "<tr bgcolor=#E7AE66>";
		print "  <td width=300 align=center><b>Field</b></td>";
		print "  <td width=400 align=center><b>Value</b></td>";
		print "</tr>";
		$row = $db->query("SELECT * FROM donation where donation_id = '$donationid'")->fetch(PDO::FETCH_ASSOC);
		print "<tr>";
		print "  <td><b>Donation Id</b></td>";
		print "  <td>".$row['donation_id']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Donation Category</b></td>";
		print "  <td>".$row['donationcategories_id']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Donation Restricted Sub-Category</b></td>";
		print "  <td>".$row['donationrestrictedsubcat_id']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Donation Date</b></td>";
		print "  <td>".$row['donationdate']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Contribution Source</b></td>";
		print "  <td>".$row['contributionsource_id']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Contribution Other</b></td>";
		print "  <td>".$row['contributionother']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Source Identifier</b></td>";
		print "  <td>".$row['sourceidentifier']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Contribution Amount</b></td>";
		print "  <td>".number_format($row['contributionamount'],2)."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Inkind Actual Value</b></td>";
		print "  <td>".number_format($row['inkind_actualvalue'],2)."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td><b>Description</b></td>";
		print "  <td>".$row['description']."</td>";
		print "</tr>";		
		print "</table><br/>";
		
		// close the database connection
		$db = NULL;
	  }
	  catch(PDOException $e)
	  {
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	  }
  
  } // End - process record
 
}
require('prj_footer.php');
?>
