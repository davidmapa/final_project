<!doctype html>
<?php

require('prj_functions.php');
require('prj_values.php');
html_head("Add Donation");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
	
?>

  <!-- Display a form to capture information -->
  <h2>Add a donation</h2>
  <form action="prj_donation.php" method="post">
    <table id="add" border="0" cellpadding="10">
	  <tr>
		<td align="left" colspan="11"><p>A <b>donation</b> is a gift given by physical or legal persons, typically for charitable purposes and/or to benefit a cause. 
		A donation may take various forms, including cash offering, services, new or used goods including clothing, toys, food, and vehicles.</p> </td>		
	  </tr>
      <tr bgcolor="#E7AE66">
        <td width="300" align="center"><b>Field</b></td>
        <td width="400" align="center"><b>Value</b></td>
      </tr>
      <tr>
        <td bgcolor="#E7AE66"><b>Donor</b></td>
		<td align="left">
			<select name="donorid">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the donor table
					$result = $db->query('SELECT * FROM donor ORDER BY donor_id');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['donor_id'].">".$row['firstname']." ".$row['lastname']." ".$row['companydonorname']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>			
		</td>
      </tr>
	  <tr>
	    <td bgcolor="#E7AE66"><b>Donation Category</b></td>
        <td align="left">
			<select name="donationcategory">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the donation category table
					$result = $db->query('SELECT * FROM donationcategories ORDER BY donationcategories');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['donationcategories_id'].">".$row['donationcategories']."</option>";					  
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
			&nbsp;&nbsp;<a href="prj_catinfo.html" onclick="return popitup('prj_catinfo.html')">Link to explanation</a>
		</td>
      </tr>
	   
	  <tr>
        <td bgcolor="#E7AE66"><b>Donation Sub-Category</b></td>
        <td align="left">
		  
			<select name="donationsubcategory">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
					//display all types in the donation sub-category table
					$result = $db->query('SELECT * FROM donationrestrictedsubcat 
										  ORDER BY donationrestrictedsubcat');
					
				    ?>
				
					<option selected disabled hidden>Choose here</option>
					
					<?php
								
					foreach($result as $row)
					{
					  print "<option value=".$row['donationrestrictedsubcat_id'].">".$row['donationrestrictedsubcat']."</option>";
					}
					
					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>		  
		  &nbsp;&nbsp;A Sub-Category is associated to restricted funds.  
		</td>
		
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Donation Date (yyyy-mm-dd)</b></td>
        <td align="left"><input type="date" name="donationdate" size="10" maxlength="10"></td>
      </tr>		  
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Source</b></td>
		<td align="left">
			<select name="contributionsource">		 
				<?php
				  // Replace text field with a select pull down menu.
				  try
				  {
					//open the database
					$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					//display all types in the contribution source table
					$result = $db->query('SELECT * FROM contributionsource Order By contributionsource');
					
				?>
				
					<option selected disabled hidden>Choose here</option>
					
				<?php
				
					foreach($result as $row)
					{
					  print "<option value=".$row['contributionsource_id'].">".$row['contributionsource']."</option>";
					}

					// close the database connection
					$db = NULL;
				  }

				  catch(PDOException $e)
				  {
					echo 'Exception : '.$e->getMessage();
					echo "<br/>";
					$db = NULL;
				  }
				?>
			</select>
            &nbsp;&nbsp;If you select <b>other</b> please explain in <b>Contribution Other</b>
		</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Other</b></td>
        <td align="left"><input type="text" name="contributionother" size="50" maxlength="100"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Source Identifier</b></td>
        <td align="left"><input type="text" name="sourceidentifier" size="50" maxlength="100">
		&nbsp;&nbsp;Include Check Number, last 4 of Credit Card, etc.</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Contribution Amount</b></td>
        <td align="left"><input type="number" name="contributionamount" size="10" maxlength="10"></td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Inkind Value Amount</b></td>
		<td align="left"><input type="number" name="inkindamount" size="10" maxlength="10">
		&nbsp;&nbsp;Gift actual value amount</td>
      </tr>
	  <tr>
        <td bgcolor="#E7AE66"><b>Description</b></td>
		<td align="left"><textarea name="description" placeholder="Enter remark up to 1000 characters" maxlength="1000" rows="5" cols="80"></textarea></td>                
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="Add Donation"></td>
      </tr>
    </table><br />
  </form>
<?php
		
} else {
  # Process the information from the form displayed
  $donorid = $_POST['donorid'];
  $donationcategory = $_POST['donationcategory'];
  $donationsubcategory = $_POST['donationsubcategory'];
  $donationdate = $_POST['donationdate'];
  $contributionsource = $_POST['contributionsource'];
  $contributionother = $_POST['contributionother'];
  $sourceidentifier = $_POST['sourceidentifier'];
  $contributionamount = $_POST['contributionamount'];
  $inkindamount = $_POST['inkindamount'];
  $description = $_POST['description'];
  
  //clean up and validate data
  $donorid = trim($donorid);
  if ( empty($donorid) ) {
    try_again("Donor field cannot be empty. Please select a donor from the dropdown.");
  }
  $donationcategory = trim($donationcategory);
  if ( empty($donationcategory) ) {
    try_again("Donation category field cannot be empty. Please select a donation category from the dropdown.");
  }
  if ($donationcategory == 2){
	  $donationsubcategory = trim($donationsubcategory);
	  if ( empty($donationsubcategory) ) {
		try_again("Donation sub-category cannot be empty for a restricted account. Please select a donation sub-category from the dropdown.");
	  }
	  if ( $donationsubcategory == 1 ) {
		try_again("Donation sub-category cannot be set to N/A. Please select another donation sub-category besides N/A from the dropdown.");
	  }	  
  }
  $donationdate = trim($donationdate);
  if ( empty($donationdate) ) {
    try_again("Donation date field cannot be empty. Please enter a donation date.");
  }
  $contributionsource = trim($contributionsource);
  if ( empty($contributionsource) ) {
    try_again("Donation source field cannot be empty. Please enter a donation source.");
  }
  if ($donationcategory != 3){  // Entered for all categories except for gifts
	  $contributionamount = trim($contributionamount);
	  if ( empty($contributionamount) ) {
		try_again("Contribution amount field cannot be empty. Please enter a contribution amount.");
	  }
  }
  if ($donationcategory == 3){  // Entered for gift category
	  $inkindamount = trim($inkindamount);
	  if ( empty($inkindamount) ) {
		try_again("Inkind amount field cannot be empty for a gift catedgory. Please enter an inkind amount.");
	  }
  }
  
  $inkindamount = trim($inkindamount);
  if ( empty($inkindamount) ) {
	$inkindamount = 0.0;
  }
	  
  $description = trim($description);
  if ( empty($description) ) {
    try_again("Description field cannot be empty. Please enter a description.");
  }
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
	//insert data...
    $db->exec("INSERT INTO donation (donor_id, donationcategories_id, donationrestrictedsubcat_id, donationdate, contributionsource_id, 
	           contributionother, sourceidentifier, contributionamount, inkind_actualvalue, description ) 
	           VALUES ('$donorid', '$donationcategory', '$donationsubcategory', '$donationdate', '$contributionsource', 
			   '$contributionother', '$sourceidentifier', '$contributionamount', '$inkindamount', '$description' );");
  
    //get the last id value inserted into the table
    $last_id = $db->lastInsertId();
	
	//now output the data from the insert to a simple html table...
    print "<h2>Donation Added</h2>";
    print "<table border=1>";
    print "<tr bgcolor=#E7AE66>";
	print "  <td width=300 align=center><b>Field</b></td>";
    print "  <td width=400 align=center><b>Value</b></td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM donation where donation_id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Donation Id</b></td>";
    print "  <td>".$row['donation_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Donation Category</b></td>";
    print "  <td>".$row['donationcategories_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Donation Sub-category</b></td>";
    print "  <td>".$row['donationrestrictedsubcat_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Donation Date</b></td>";
    print "  <td>".$row['donationdate']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Contribution Source Id</b></td>";
    print "  <td>".$row['contributionsource_id']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Contribution Other Source</b></td>";
    print "  <td>".$row['contributionother']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Source Identifier</b></td>";
    print "  <td>".$row['sourceidentifier']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Contribution Amount</b></td>";
    print "  <td>".$row['contributionamount']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Inkind Value Amount</b></td>";
    print "  <td>".$row['inkind_actualvalue']."</td>";
	print "</tr>";
	print "<tr>";
	print "  <td bgcolor=#E7AE66><b>Description</b></td>";
    print "  <td>".$row['description']."</td>";
	print "</tr>";    
    print "</table>";
	
    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
 
}
require('prj_footer.php');
?>
