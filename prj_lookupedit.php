<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("Edit Lookup");
require('prj_header.php');
require('prj_sidebar.php');


# Code for your web page follows.
if (!isset($_POST['submit']))
{  
	
	$luaddtype =  $_GET['type'];
	$luid =  $_GET['id']; 
	  
	// Open lookup tables at a specific record
	try
	{
	  
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
		if ( $luaddtype == 1 ) {
		
			$editarec = "Edit a State record";
			
			$sql="SELECT statecode, state
				  FROM states			    
				  WHERE UPPER(statecode) = UPPER('$luid');";
			
			$result = $db->query($sql);
			foreach($result as $row) {
				$statecode = $row['statecode'];
				$state = $row['state'];				
			}
					
		} elseif ( $luaddtype == 2 ) {
		
			$editarec = "Edit a Category record";
			
			$sql="SELECT donationcategories_id,
			        donationcategories, 
					description,
					active_id,
					activedate
				  FROM donationcategories			    
				  WHERE donationcategories_id = $luid;";
			
			$result = $db->query($sql);
			foreach($result as $row) {
				$id = $row['donationcategories_id'];
				$thevalue = $row['donationcategories'];
				$desc = $row['description'];
				$status = $row['active_id'];
				$actdate = $row['activedate'];
			}			
			
		} elseif ( $luaddtype == 3 ) {
		
			$editarec = "Edit a Sub-Category record";
			
			$sql="SELECT donationrestrictedsubcat_id,
			        donationrestrictedsubcat, 
					description,
					active_id,
					activedate
				  FROM donationrestrictedsubcat			    
				  WHERE donationrestrictedsubcat_id = $luid;";
			
			$result = $db->query($sql);
			foreach($result as $row) {
				$id = $row['donationrestrictedsubcat_id'];
				$thevalue = $row['donationrestrictedsubcat'];
				$desc = $row['description'];
				$status = $row['active_id'];
				$actdate = $row['activedate'];
			}
			
		} elseif ( $luaddtype == 4 ) {
		
			$editarec = "Edit a Contribution Source record";
			
			$sql="SELECT contributionsource_id,
			        contributionsource, 
					description,
					active_id,
					activedate
				  FROM contributionsource			    
				  WHERE contributionsource_id = $luid;";
				  
			$result = $db->query($sql);
			foreach($result as $row) {
				$id = $row['contributionsource_id'];
				$thevalue = $row['contributionsource'];
				$desc = $row['description'];
				$status = $row['active_id'];
				$actdate = $row['activedate'];
			}	  
			
		} else {

			$editarec = "Edit a Donor Type record";
			
			$sql="SELECT donortype_id,
			        donortype, 
					description,
					active_id,
					activedate
				  FROM donortype			    
				  WHERE donortype_id = $luid;";
				  
			$result = $db->query($sql);
			foreach($result as $row) {
				$id = $row['donortype_id'];
				$thevalue = $row['donortype'];
				$desc = $row['description'];
				$status = $row['active_id'];
				$actdate = $row['activedate'];
			}	  
			
		}
		
		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}
	
	echo "<h2>" . $editarec . "</h6>";  
	
	print "<form action=prj_lookupedit.php method=post>";
	
	if ( $luaddtype == 1 ) {  // Edit State
					
?>
  
		<!-- Display a form to capture information -->    
		<!--<form action="prj_lookupedit.php" method="post">-->
		<table border="0">
			<tr bgcolor="#E7AE66">
				<td width="200" align="center"><b>Statecode</b></td>
				<td width="400" align="center"><b>State</b></td>		
			</tr>
			<tr>
				<td align="left"><input  style="border:none;font-size: 16px;font-weight: bold" type="text" name="statecode" value="<?php echo $statecode;?>" readonly></td>
				<td align="left"><input type="text" name="state" size="25" maxlength="50" value="<?php echo $state;?>">	
			</tr>
	        <tr>
				<td align="center"><b>Record Process Type</b></td>
				<td align="left"><input type="radio" name="group1" value="deleterow">Delete<br />
			                 	 <input type="radio" name="group1" value="updaterow" checked>Update</td><br />
			</tr>
	  
<?php

	} else {   // Edit all others beside states
	  
?>

	    <table border="0">
			<tr bgcolor="#E7AE66">
				<td width="200" align="center"><b>Id</b></td>
				<td width="400" align="center"><b>Lookup Value</b></td>					
			</tr>
			<tr>
				<td width="200" align="left"><input  style="border:none;font-size: 16px;font-weight: bold" type="text" name="id" value="<?php echo $id;?>" readonly></td>
				<td width="400" align="left"><input type="text" name="value" size="15" maxlength="50" value="<?php echo $thevalue;?>">	
			</tr>			
			<tr bgcolor="#E7AE66">
				<td width="600" align="center" colspan="2"><b>Value Description</b></td>				
			</tr>
			
			<tr>
				<td align="left" colspan="2"><input type="text" name="description" size="80" maxlength="100" value="<?php echo $desc;?>">	
			</tr>
			<tr bgcolor="#E7AE66">
				<td width="300" align="center"><b>Status</b></td>
				<td width="300" align="center"><b>Date</b></td>
			</tr>
			<tr>
				<td width="300" align="left">
					<select name="statusid">		 
					<?php
					  // Replace text field with a select pull down menu.
					  try
					  {
						//open the database
						$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
						$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

						//display all types in the types table
						$result = $db->query('SELECT * FROM active order by active');
						
					?>
				
							<option selected disabled hidden>Choose here</option>
					
					<?php
				
						foreach($result as $row)
						{
						  print "<option value=".$row['active_id'].">".$row['active']."</option>";
						}

						// close the database connection
						$db = NULL;
					  }

					  catch(PDOException $e)
					  {
						echo 'Exception : '.$e->getMessage();
						echo "<br/>";
						$db = NULL;
					  }
					?>
					</select>
				</td>
				<td width="300" align="left"><input type="date" name="statusdate" size="10" maxlength="10" value="<?php echo $actdate;?>"></td>	
			</tr>
	        <tr>
				<td align="center" bgcolor="#E7AE66"><b>Record Process Type</b></td>
				<td align="left"><input type="radio" name="group1" value="deleterow">Delete<br />
			                 	 <input type="radio" name="group1" value="updaterow" checked>Update</td><br />
			</tr>
			
<?php
	}
?>			
			
			
			<tr>
				<td colspan="2" align="center"><input type="number" name="luaddtype" value="<?php echo $luaddtype;?>" hidden ></td>
			</tr>
      <tr>
        <td colspan="3" align="center"><input type="submit" name="submit" value="Process Record"></td>
      </tr>
    </table><br />
	</form>
<?php
} else {
	# Process the information from the form displayed
	$luaddtype = $_POST['luaddtype'];
	
	//print "<h4>Type ". $luaddtype . "</h4>";
	  
	if ( $luaddtype == 1 ) {  // Begin - This is to process a states record
	  
		$statecode = $_POST['statecode'];  
		$state = $_POST['state'];  
		$process = $_POST['group1']; 
		
		// Determine to delete or update the states record
		if ( $process == 'deleterow' ) {
			
			//Determine if state is already being used in a donor record			
			
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				//Get count of a statecode in the donor table
				$result = $db->query("SELECT count(*) AS rowcount 
				                      FROM donor 
									  WHERE UPPER(statecode) = UPPER('$statecode')");
						
				foreach($result as $row)
				{
				  $rowcount = $row['rowcount'];
				}


				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}	

			//Check if Statecode exist
			if ( $rowcount > 0 ) {
			try_again("Contegrity constraint error: You can not delete this state code.  The Statecode of [ ". $statecode ." ] is being used in at least one row in the donor table. The donor record(s) 
			           would need to be deleted first...");
			}
			
			//	Process delete request		
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//Delete record
				$db->exec("DELETE FROM states WHERE UPPER(statecode) = UPPER('$statecode');");
				
				print "<h2>States Record Deleted</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Statecode deleted</b></td>";
				print "  <td>".$statecode."</td>";
				print "</tr>";
				print "</table><br/>";
						
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
					
		} else {  // Update states record
		
			//clean up and validate state code
			$statecode = trim($statecode);
			if ( empty($statecode) ) {
			  try_again("State code field cannot be empty. Please enter a state code.");
			}
			
			//Check length
			if ( strlen($statecode) <> 2 ) {
			  try_again("State code field must be two characters. Please enter a two character state code.");
			}
			
			//clean up and validate state
			$state = trim($state);
			if ( empty($state) ) {
			  try_again("State field cannot be empty. Please enter a state.");
			}
		
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//Update record
				$db->exec("UPDATE states 
							 SET state = '$state'
						   WHERE UPPER(statecode) = UPPER('$statecode');");
			
				print "<h2>States Record Updated</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				$row = $db->query("SELECT * FROM states where UPPER(statecode) = UPPER('$statecode')")->fetch(PDO::FETCH_ASSOC);
				print "<tr>";
				print "  <td><b>Statecode</b></td>";
				print "  <td>".$row['statecode']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>State</b></td>";
				print "  <td>".$row['state']."</td>";
				print "</tr>";			
				print "</table><br/>";
			
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
		}	// End if Delete/Update for States
	  
	}  // End - This is to process a states record  	
	
  	
	if ( $luaddtype == 2 ) {  // Begin - Delete or Update a Donation Category lookup record
	
		$id = $_POST['id'];  
		$value = $_POST['value'];  
		$description = $_POST['description']; 
		$status = $_POST['statusid']; 
		$statusdate = $_POST['statusdate']; 
		$process = $_POST['group1']; 		
		
		// Determine to delete or update the category record
		if ( $process == 'deleterow' ) {  			

			//Determine if the category is already being used in a donation record					
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				//Get count of a donation category in the donation table
				$result = $db->query("SELECT count(*) AS rowcount 
				                      FROM donation
									  WHERE donationcategories_id = $id;");
						
				foreach($result as $row)
				{
				  $rowcount = $row['rowcount'];
				}


				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}	

			//Check if category exist
			if ( $rowcount > 0 ) {
			try_again("Contegrity constraint error: You can not delete this donation category.  The donation category of [ ". $value ." ] is being used in at least one row in the donation table. The donation record(s) 
			           would need to be deleted first...");
			}			
			
			// Process delete request
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//Delete record
				$db->exec("DELETE FROM donationcategories WHERE donationcategories_id = $id;");
				
				print "<h2>Category Record Deleted</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Category Id deleted</b></td>";
				print "  <td>".$id."</td>";
				print "</tr>";
				print "</table><br/>";
						
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
					
		} else {  // Update donation category record
		
			$value = trim($value);
			if ( empty($value) ) {
			  try_again("Category field cannot be empty. Please enter a category.");
			}
			
			$description = trim($description);
			if ( empty($description) ) {
			  try_again("Category description field cannot be empty. Please enter a category description.");
			}
			
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//Update record
				$db->exec("UPDATE donationcategories 
							 SET donationcategories = '$value',
							     description = '$description',
								 active_id = '$status',
								 activedate = '$statusdate'
						   WHERE donationcategories_id = $id;");
			
				print "<h2>Donation Category Record Updated</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				$row = $db->query("SELECT * 
				                   FROM donationcategories 
								   where donationcategories_id = $id")->fetch(PDO::FETCH_ASSOC);
				print "<tr>";
				print "  <td><b>Id</b></td>";
				print "  <td>".$row['id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Category</b></td>";
				print "  <td>".$row['donationcategories']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Description</b></td>";
				print "  <td>".$row['description']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Status</b></td>";
				print "  <td>".$row['active_id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Date</b></td>";
				print "  <td>".$row['activedate']."</td>";
				print "</tr>";
				
				print "</table><br/>";
			
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
		} // End if Delete/Update
	
	} // End - Delete or Update a Donation Category lookup record
	
	if ( $luaddtype == 3 ) {  // Begin - Delete or Update a Donation Restricted Sub-Category lookup record

		$id = $_POST['id'];  
		$value = $_POST['value'];  
		$description = $_POST['description']; 
		$status = $_POST['statusid']; 
		$statusdate = $_POST['statusdate']; 
		$process = $_POST['group1']; 		
		
		// Determine to delete or update the donation restricted subcat record
		if ( $process == 'deleterow' ) {  

			//Determine if the sub-category is already being used in a donation record					
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				//Get count of a donation sub-category in the donation table
				$result = $db->query("SELECT count(*) AS rowcount 
				                      FROM donation
									  WHERE donationrestrictedsubcat_id = $id;");
						
				foreach($result as $row)
				{
				  $rowcount = $row['rowcount'];
				}


				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}	

			//Check if sub-category exist
			if ( $rowcount > 0 ) {
			try_again("Contegrity constraint error: You can not delete this donation restricted sub-category.  The donation restricted sub-category of [ ". $value ." ] is being used in at least one row in the donation table. The donation record(s) 
			           would need to be deleted first...");
			}
		
			// Process the delete request
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//delete record
				$db->exec("DELETE FROM donationrestrictedsubcat WHERE donationrestrictedsubcat_id = $id;");
				
				print "<h2>Restricted Sub-Category Record Deleted</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Category Id deleted</b></td>";
				print "  <td>".$id."</td>";
				print "</tr>";
				print "</table><br/>";
						
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
					
		} else {  // Update donation restricted sub-category record
		
			$value = trim($value);
			if ( empty($value) ) {
			  try_again("Restricted sub-category field cannot be empty. Please enter a restricted sub-category.");
			}
			
			$description = trim($description);
			if ( empty($description) ) {
			  try_again("Restricted sub-category description field cannot be empty. Please enter a restricted sub-category description.");
			}		
		
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//update record
				$db->exec("UPDATE donationrestrictedsubcat 
							 SET donationrestrictedsubcat = '$value',
							     description = '$description',
								 active_id = '$status',
								 activedate = '$statusdate'
						   WHERE donationrestrictedsubcat_id = $id;");
			
				print "<h2>Donation Restricted Sub-Category Record Updated</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				$row = $db->query("SELECT * 
				                   FROM donationrestrictedsubcat 
								   where donationrestrictedsubcat_id = $id")->fetch(PDO::FETCH_ASSOC);
				print "<tr>";
				print "  <td><b>Id</b></td>";
				print "  <td>".$row['id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Restricted Sub-Category</b></td>";
				print "  <td>".$row['donationrestrictedsubcat']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Description</b></td>";
				print "  <td>".$row['description']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Status</b></td>";
				print "  <td>".$row['active_id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Date</b></td>";
				print "  <td>".$row['activedate']."</td>";
				print "</tr>";
				
				print "</table><br/>";
			
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
		}  // End if Delete/Update		
		
	} // End - Delete or Update a Donation Restricted Sub-Category lookup record
	
	if ( $luaddtype == 4 ) {  // BEGIN - Delete or Update a Contribution Source lookup record
	
		$id = $_POST['id'];  
		$value = $_POST['value'];  
		$description = $_POST['description']; 
		$status = $_POST['statusid']; 
		$statusdate = $_POST['statusdate']; 
		$process = $_POST['group1'];
							
		// Determine to delete or update the contribution source record
		if ( $process == 'deleterow' ) {  
		
			//Determine if the contribution souirce is already being used in a donation record					
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				//Get count of a contribution source in the donation table
				$result = $db->query("SELECT count(*) AS rowcount 
				                      FROM donation
									  WHERE contributionsource_id = $id;");
						
				foreach($result as $row)
				{
				  $rowcount = $row['rowcount'];
				}


				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}	

			//Check if sub-category exist
			if ( $rowcount > 0 ) {
			try_again("Contegrity constraint error: You can not delete this contribution source.  The contribution source of [ ". $value ." ] is being used in at least one row in the donation table. The donation record(s) 
			           would need to be deleted first...");
			}		
			
			// Process delete request
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//delete record
				$db->exec("DELETE FROM contributionsource WHERE contributionsource_id = $id;");
				
				print "<h2>Contribution Source Record Deleted</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Contribution Source Id deleted</b></td>";
				print "  <td>".$id."</td>";
				print "</tr>";
				print "</table><br/>";
						
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
					
		} else {  // Update contribuition source record
		
			$value = trim($value);
			if ( empty($value) ) {
			  try_again("Contribution Source field cannot be empty. Please enter a contribution source.");
			}
			
			$description = trim($description);
			if ( empty($description) ) {
			  try_again("Contribution source description field cannot be empty. Please enter a contribution source description.");
			}
		
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//update record
				$db->exec("UPDATE contributionsource 
							 SET contributionsource = '$value',
							     description = '$description',
								 active_id = '$status',
								 activedate = '$statusdate'
						   WHERE contributionsource_id = $id;");
			
				print "<h2>Contribution Source Record Updated</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				$row = $db->query("SELECT * 
				                   FROM contributionsource 
								   where contributionsource_id = $id")->fetch(PDO::FETCH_ASSOC);
				print "<tr>";
				print "  <td><b>Id</b></td>";
				print "  <td>".$row['id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Restricted Sub-Category</b></td>";
				print "  <td>".$row['contributionsource']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Description</b></td>";
				print "  <td>".$row['description']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Status</b></td>";
				print "  <td>".$row['active_id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Date</b></td>";
				print "  <td>".$row['activedate']."</td>";
				print "</tr>";
				
				print "</table><br/>";
			
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
		}  // End if Delete/Update
		
	} // END - Delete or Update a Contribution Source lookup record
	
	
	if ( $luaddtype == 5 ) {  // Begin - Delete or Update a Donor Type lookup record
	
		$id = $_POST['id'];  
		$value = $_POST['value'];  
		$description = $_POST['description']; 
		$status = $_POST['statusid']; 
		$statusdate = $_POST['statusdate']; 
		$process = $_POST['group1'];
						
		// Determine to delete or update the donor type record
		if ( $process == 'deleterow' ) {  
		
			//Determine if the donor type is already being used in a donor record					
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				//Get count of a donor type in the donor table
				$result = $db->query("SELECT count(*) AS rowcount 
				                      FROM donor
									  WHERE donortype_id = $id;");
						
				foreach($result as $row)
				{
				  $rowcount = $row['rowcount'];
				}


				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}	

			//Check if sub-category exist
			if ( $rowcount > 0 ) {
			try_again("Contegrity constraint error: You can not delete this donor type.  The donor type of [ ". $value ." ] is being used in at least one row in the donor table. The donor record(s) 
			           would need to be deleted first...");
			}		
			
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//delete record
				$db->exec("DELETE FROM donortype WHERE donortype_id = $id;");
				
				print "<h2>Donor Type Record Deleted</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Donor Type Id deleted</b></td>";
				print "  <td>".$id."</td>";
				print "</tr>";
				print "</table><br/>";
						
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
					
		} else {  // Update donor type record
		
			$value = trim($value);
			if ( empty($value) ) {
			  try_again("Donor type field cannot be empty. Please enter a donor type.");
			}
			
			$description = trim($description);
			if ( empty($description) ) {
			  try_again("Donor type description field cannot be empty. Please enter a donor type description.");
			}
		
			try
			{
				//open the database
				$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
				//update record
				$db->exec("UPDATE donortype 
							 SET donortype = '$value',
							     description = '$description',
								 active_id = '$status',
								 activedate = '$statusdate'
						   WHERE donortype_id = $id;");
			
				print "<h2>Donor Type Record Updated</h2>";
				print "<table border=1>";
				print "<tr bgcolor=#E7AE66>";
				print "  <td width=300 align=center><b>Field</b></td>";
				print "  <td width=400 align=center><b>value</b></td>";
				print "</tr>";
				$row = $db->query("SELECT * 
				                   FROM donortype 
								   where donortype_id = $id")->fetch(PDO::FETCH_ASSOC);
				print "<tr>";
				print "  <td><b>Id</b></td>";
				print "  <td>".$row['id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Donor Type</b></td>";
				print "  <td>".$row['donortype']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Description</b></td>";
				print "  <td>".$row['description']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Status</b></td>";
				print "  <td>".$row['active_id']."</td>";
				print "</tr>";
				print "<tr>";
				print "  <td><b>Date</b></td>";
				print "  <td>".$row['activedate']."</td>";
				print "</tr>";
				
				print "</table><br/>";
			
				// close the database connection
				$db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
		}	// End if Delete/Update

	}  // End - Delete or Update a Donor Type lookup record
  
 
}
require('prj_footer.php');
?>
