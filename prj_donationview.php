<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("View Donation");
require('prj_header.php');
require('prj_sidebar.php');

$donationidin =  $_GET['id'];
  
// Open donor table at a specific record
try
{
  
	//open the database
	$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
?>

<h2>View details of a donors donation</h2>
<table border="0" cellpadding="10">
      <tr bgcolor="#E7AE66">
        <td width="200" align="center"><b>Field</b></td>
        <td width="500" align="center"><b>Actual Value</b></td>		
      </tr

 <?php	
	$sql="SELECT dn.donation_id as 'donation_id', 
	           trim(CONCAT(dr.firstname, ' ', dr.lastname)) as 'donorname',
			   dr.companydonorname as 'companyname',
			   dnc.donationcategories as 'donationcategories',
			   dnrsc.donationrestrictedsubcat as 'donationrestictedsubcat',
			   dn.donationdate as 'donationdate',
			   cs.contributionsource as 'contributionsource',
			   dn.contributionother as 'contributionother',
			   dn.sourceidentifier as 'sourceidentifier',
			   dn.contributionamount as 'contributionamount',
			   dn.inkind_actualvalue as 'inkind_actualvalue',
			   dn.description
			FROM donation as dn
			  LEFT JOIN donor as dr ON dn.donor_id = dr.donor_id
			  LEFT JOIN donationcategories as dnc ON dn.donationcategories_id = dnc.donationcategories_id
			  LEFT JOIN donationrestrictedsubcat as dnrsc ON dn.donationrestrictedsubcat_id = dnrsc.donationrestrictedsubcat_id
			  LEFT JOIN contributionsource as cs ON dn.contributionsource_id  = cs.contributionsource_id			  
			WHERE  dn.donation_id = $donationidin;";
			
	$result = $db->query($sql);
	foreach($result as $row) {
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Id</b></td>";
		print "  <td>".$row['donation_id']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donors Name</b></td>";
		print "  <td>".$row['donorname']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donors Organization</b></td>";
		print "  <td>".$row['companyname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Category</b></td>";
		print "  <td>".$row['donationcategories']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Restricted Sub-Category</b></td>";
		print "  <td>".$row['donationrestictedsubcat']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donation Date</b></td>";
		print "  <td>".$row['donationdate']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Contribution Source</b></td>";
		print "  <td>".$row['contributionsource']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Contribution Other</b></td>";
		print "  <td>".$row['contributionother']."</td>";
		print "</tr>";		
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Source Identifier</b></td>";
		print "  <td>".$row['sourceidentifier']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Contribution Amount</b></td>";
		print "  <td>".number_format($row['contributionamount'],2)."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Contribution Inkind Value Amount</b></td>";
		print "  <td>".number_format($row['inkind_actualvalue'],2)."</td>";
		print "</tr>";		
		print "<tr>";
		print "  <td colspan=2 align=center bgcolor=#E7AE66><b>Remarks</b></td>";
		print "</tr>";
		print "<tr>";
		print "  <td colspan=2>".$row['description']."</td>";
		print "</tr>";								
			
	}
	
	print "</table><br />";	
	
	// close the database connection
	$db = NULL;
    }

	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
  }
require('prj_footer.php');	
?>

