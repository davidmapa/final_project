<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("View Donor");
require('prj_header.php');
require('prj_sidebar.php');

$donoridin =  $_GET['id'];
  
// Open donor table at a specific record
try
{
  
	//open the database
	$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
?>

<h2>View details of a donor</h2>
<table border="0" cellpadding="10">
      <tr bgcolor="#E7AE66">
        <td width="200" align="center"><b>Field</b></td>
        <td width="500" align="center"><b>Actual Value</b></td>		
      </tr

 <?php	
	$sql="SELECT d.donor_id as 'donor_id', 
			   dt.donortype as 'donortype', 
			   d.firstname as 'firstname',
			   d.lastname as 'lastname', 
			   d.knownbyname as 'knownbyname',
			   d.companydonorname as 'companyname', 
			   d.addressline1 as 'addressline1',
			   d.addressline2 as 'addressline2',
			   d.addressline3 as 'addressline3',
			   d.city as 'city', 
			   d.statecode as 'statecode', 
			   d.zipcode as 'zipcode', 
			   d.phone1 as 'phone1', 
			   d.phone2 as 'phone2', 
			   d.email1 as 'email1',
			   d.email2 as 'email2',
			   d.remarks as 'remarks',
			   a.active as 'active', 
			   d.activedate as 'activedate',
			   (SELECT SUM(contributionamount) FROM donation WHERE donor_id = d.donor_id) as 'sumcontributionamount',
		       (SELECT SUM(inkind_actualvalue) FROM donation WHERE donor_id = d.donor_id) as 'suminkindactualvalue'
			FROM donor as d
			  LEFT JOIN active as a ON d.active_id  = a.active_id
			  LEFT JOIN donortype as dt ON d.donortype_id = dt.donortype_id
			WHERE d.donor_id = $donoridin;";
			
	$result = $db->query($sql);
	foreach($result as $row) {
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donor Id</b></td>";
		print "  <td>".$row['donor_id']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Donor Type</b></td>";
		print "  <td>".$row['donortype']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>First Name</b></td>";
		print "  <td>".$row['firstname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Last Name</b></td>";
		print "  <td>".$row['lastname']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Known By Name</b></td>";
		print "  <td>".$row['knownbyname']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Organization Name</b></td>";
		print "  <td>".$row['companyname']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Address Line 1</b></td>";
		print "  <td>".$row['addressline1']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Address Line 2</b></td>";
		print "  <td>".$row['addressline2']."</td>";
		print "</tr>";		
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Address Line 3</b></td>";
		print "  <td>".$row['addressline3']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>City</b></td>";
		print "  <td>".$row['city']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>State</b></td>";
		print "  <td>".$row['statecode']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Zipcode</b></td>";
		print "  <td>".$row['zipcode']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Phone 1</b></td>";
		print "  <td>".$row['phone1']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Phone 2</b></td>";
		print "  <td>".$row['phone2']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Email 1</b></td>";
		print "  <td>".$row['email1']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Email 2</b></td>";
		print "  <td>".$row['email2']."</td>";
		print "</tr>";
		print "<tr>";
		print "  <td colspan=2 align=center bgcolor=#E7AE66><b>Remarks</b></td>";
		print "</tr>";
		print "<tr>";
		print "  <td colspan=2>".$row['remarks']."</td>";
		print "</tr>";	
		print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Status</b></td>";
		print "  <td>".$row['active']."</td>";
		print "</tr>";
        print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Status Date</b></td>";
		print "  <td>".$row['activedate']."</td>";
		print "</tr>";	
        print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Total Amount Donated</b></td>";
		print "  <td>".number_format($row['sumcontributionamount'],2)."</td>";
		print "</tr>";	
        print "<tr>";
		print "  <td bgcolor=#E7AE66><b>Total Inkind Value Amount Donated</b></td>";
		print "  <td>".number_format($row['suminkindactualvalue'],2)."</td>";
		print "</tr>";			
			
	}
	
	print "</table><br />";	
	
	// close the database connection
	$db = NULL;
    }

	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
   }
require('prj_footer.php');	
?>

