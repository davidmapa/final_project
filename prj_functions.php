<?php
# CSCI59P standard functions
function html_head($title) {
  echo '<html lang="en">';
  echo '<head>';
  echo '<meta charset="utf-8">';
  echo "<title>$title</title>";
  echo '<link rel="stylesheet" href="prj.css">';
  echo '</head>';
  echo '<body>';
}

function try_again($str) {
  echo $str;
  echo "<br/>";
  //the following emulates pressing the back button on a browser
  echo '<a href="#" onclick="history.back(); return false;">Try Again</a>';
  require('prj_footer.php');
  exit;
}

?>
