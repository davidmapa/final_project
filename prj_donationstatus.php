<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("prj donation status");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{

	# Code for your web page follows.
	try
	{
	   
	  //open the database
	  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
	  $sql="SELECT CONCAT(YEAR(CURDATE()),'-01-01') AS 'begindate', CURDATE() AS 'enddate';";
	  
	  $result = $db->query($sql);
	  foreach($result as $row) {
		$begindate = $row['begindate'];
		$enddate = $row['enddate'];
	  }
	  
	  // close the database connection
	  $db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
    }
	
	  
	
?>

	<h2>Donation Status Date Range and Filter</h2>
	<form action="prj_donationstatus.php" method="post">
		<table border="0" cellpadding="10">
		  <tr>
		    <td align="left" colspan="2">Enter the date range to retrieve the donation listings within the range. The default begin date is January 1 <br />
			of the current year and the end date is the current date. The default dates can be changed.</td>		
		  </tr>
		  <tr bgcolor="#E7AE66">
			<td width="300" align="center"><b>Begin Date</b></td>
			<td width="300" align="center"><b>End Date</b></td>			
		  </tr>
		  <tr>
			<td align="center"><input type="date" name="begindate" size="10" maxlength="10" value="<?php echo $begindate;?>"></td>
			<td align="center"><input type="date" name="enddate" size="10" maxlength="10" value="<?php echo $enddate;?>"></td>			
		  </tr>	
		  <tr>
		    <td align="center"><b>Retrieval Filter Type</b></td>
			<td align="left"><input type="radio" name="group1" value="all" checked> All Donors <br />
			                 <input type="radio" name="group1" value="one" > One (a specific donor)</td>
		  </tr>
          <tr>
			<td colspan="2" align="center"><b>Note:</b> If [ One ] from above was press select a specific donator below.</td>
		  </tr>		  
		  <tr>
		    <td align="center" bgcolor="#E7AE66"><b>Select Specific Donor</b></td>
			<td align="left">
				<select name="donor">		 
					<?php
					  // Replace text field with a select pull down menu.
					  try
					  {
						//open the database
						$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
						$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

						//display all types in the contributionsource table
						$result = $db->query('SELECT donor_id,
                                                CASE donortype_id
                                                  WHEN 1 THEN companydonorname
												  WHEN 2 THEN companydonorname
												  ELSE CONCAT(firstname," ",lastname)
												END AS enityname
						                      FROM donor 
											  WHERE active_id = 1 
											  ORDER BY enityname');
						foreach($result as $row)
						{
						  print "<option value=".$row['donor_id'].">".$row['enityname']."</option>";
						}

						// close the database connection
						$db = NULL;
					  }

					  catch(PDOException $e)
					  {
						echo 'Exception : '.$e->getMessage();
						echo "<br/>";
						$db = NULL;
					  }
					?>
				</select>
			</td>
		  </tr>
		  <tr>
			<td colspan="2" align="center"><input type="submit" name="submit" value="Retrieve Records"></td>
		  </tr>
		</table>
	</form><br />	
<?php
} else {
	  # Process the information from the form displayed
	  $begindate = $_POST['begindate'];
	  $enddate = $_POST['enddate'];
	  $group = $_POST['group1'];
	  $donor = $_POST['donor']; 
	  
	  // Build Report Date line
	  $reportdates = "<b>Donation status period:</b> ".$begindate." <b>through</b> ".$enddate;	
	  
?>


<h2>Donation Status</h2>
<!-- display all equipment -->
<table border=1 cellpadding="10">
<?php	
	print "<tr>";
    print "  <td colspan=9 align=center bgcolor=#E7AE66>".$reportdates."</td>"; 
    print "</tr>";
?>
  <tr>
	<td align="left" colspan="11">A <b>donation</b> is a gift given by physical or legal persons, typically for charitable purposes and/or to benefit a cause. 
		A donation may take various forms, including cash offering, services, new or used goods including clothing, toys, food, and vehicles. </td>		
  </tr>
  <tr bgcolor="#E7AE66">
    <td align="center"><b>Donation Id</b></td>
	<td align="center"><b>Name</b></td>
	<td align="center"><b>Donation Category</b></td>
	<td align="center"><b>Donation Sub-category</b></td>
	<td align="center"><b>Donation Date</b></td>	
	<td align="center"><b>Contribution Amount</b></td>
	<td align="center"><b>Inkind Amount</b></td>
	<td align="center"><b>Edit</b></td>
	<td align="center"><b>View</b></td>	
  </tr>

  <?php
  
	try
	{
	  
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		  
		$group = trim($group);
		if ( $group == 'all' ) {  // Retrieves all records
		  
			$sql="SELECT dn.donation_id as 'donationid', 
			      CASE 
			       WHEN LENGTH(TRIM(dr.companydonorname)) = 0 THEN CONCAT(dr.firstname,' ',dr.lastname)
				   ELSE TRIM(dr.companydonorname)
			      END AS 'donorname',				  
				  trim(dc.donationcategories) as 'donationcategories',
				  trim(dsc.donationrestrictedsubcat) as 'donationsubcategories',
				  dn.donationdate as 'donationdate',
				  dn.contributionamount as 'contributionamount',
				  dn.inkind_actualvalue as 'inkind_actualvalue'
				FROM donation as dn
				  LEFT JOIN donor as dr ON dn.donor_id = dr.donor_id
				  LEFT JOIN donationcategories as dc ON dn.donationcategories_id = dc.donationcategories_id
				  LEFT JOIN donationrestrictedsubcat as dsc ON dn.donationrestrictedsubcat_id = dsc.donationrestrictedsubcat_id
				WHERE dn.donationdate BETWEEN '$begindate' AND '$enddate'
				ORDER BY dr.donor_id, donationdate;";
				
		} else {  // Retrievs records from a specific donor

			$sql="SELECT dn.donation_id as 'donationid', 
				  CASE 
			       WHEN LENGTH(TRIM(dr.companydonorname)) = 0 THEN CONCAT(dr.firstname,' ',dr.lastname)
				   ELSE TRIM(dr.companydonorname)
			      END AS 'donorname',
				  trim(dc.donationcategories) as 'donationcategories',
				  trim(dsc.donationrestrictedsubcat) as 'donationsubcategories',
				  dn.donationdate as 'donationdate',
				  dn.contributionamount as 'contributionamount',
				  dn.inkind_actualvalue as 'inkind_actualvalue'
				FROM donation as dn
				  LEFT JOIN donor as dr ON dn.donor_id = dr.donor_id
				  LEFT JOIN donationcategories as dc ON dn.donationcategories_id = dc.donationcategories_id
				  LEFT JOIN donationrestrictedsubcat as dsc ON dn.donationrestrictedsubcat_id = dsc.donationrestrictedsubcat_id
				WHERE dn.donationdate BETWEEN '$begindate' AND '$enddate'
				  AND dn.donor_id = '$donor'
				ORDER BY dr.donor_id, donationdate;";
		}
  
	    $result = $db->query($sql);
	    foreach($result as $row) {
			print "<tr>";
			print "  <td><b>".$row['donationid']."</b></td>";
			print "  <td>".$row['donorname']."</td>";
			print "  <td>".$row['donationcategories']."</td>";
			print "  <td>".$row['donationsubcategories']."</td>";
			print "  <td>".$row['donationdate']."</td>";	
			print "  <td align=right>".number_format($row['contributionamount'],2)."</td>";	
			print "  <td align=right>".number_format($row['inkind_actualvalue'],2)."</td>";
			print "  <td><a href='prj_donationedit.php?id=".$row['donationid']."'>click to edit</a></td>";
			print "  <td><a href='prj_donationview.php?id=".$row['donationid']."'>click to view</a></td>";
			print "</tr>";
	    }
		
		if ( $group == 'all' ) { 
  
			$sql2="SELECT SUM(contributionamount) as 'sumcontributionamount',
                   SUM(inkind_actualvalue) as 'suminkindactualvalue'
				   FROM donation
				   WHERE donationdate BETWEEN '$begindate' AND '$enddate';";
			
		} else {
			
			$sql2="SELECT SUM(contributionamount) as 'sumcontributionamount',
                   SUM(inkind_actualvalue) as 'suminkindactualvalue'
				   FROM donation
				   WHERE donationdate BETWEEN '$begindate' AND '$enddate'
				   AND donor_id = '$donor';";			
		
		}
		 
		$result2 = $db->query($sql2);
		foreach($result2 as $row) {
			print "<tr>";
			print "  <td colspan=5 align=center bgcolor=#E7AE66><b>Contribution Total</b></td>";
			print "  <td align=right><b>".number_format($row['sumcontributionamount'],2)."</b></td>";
			print "  <td align=right><b>".number_format($row['suminkindactualvalue'],2)."</b></td>";
			print "  <td></td>";
			print "  <td></td>";
			print "</tr>";
		}	

		print "</table><br />";

	  // close the database connection
	  $db = NULL;
	}
	catch(PDOException $e)
	{
	  echo 'Exception : '.$e->getMessage();
	  echo "<br/>";
	  $db = NULL;
	}
}	
	
require('prj_footer.php');
?>
