<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("prj lookup status");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{

?>

	<h2>Lookup Table Status For Maintenance</h2>
	<form action="prj_lookuptablestatus.php" method="post">
		<table border="0" cellpadding="10">
		  <tr>
		    <td align="left" colspan="2">This is the section for lookup table maintenance.  Lookup table data is used in<br />
			   the dropdowns for the donor and donation add and edit pages.   </td>		
		  </tr>
		  <tr>
		    <td align="center"><b>Table Filter Type</b></td>
			<td align="left"><input type="radio" name="group1" value="states" checked> States table<br />
			                 <input type="radio" name="group1" value="category"> Category table<br />
							 <input type="radio" name="group1" value="subcategory"> Sub-Category table<br />
							 <input type="radio" name="group1" value="contributionsource"> Contribution source table<br />
							 <input type="radio" name="group1" value="donortype"> Donor type table</td><br />
		  </tr>
          <tr>
			<td colspan="2" align="center"><input type="submit" name="submit" value="Submit"></td>
		  </tr>
		</table>
	</form><br />	
<?php
} else {
	  # Process the information from the form displayed
	  $group = $_POST['group1'];
	  
	  if ( $group == 'states' ) {  // Retrieves states rows
	     $reporttitle = "<b>Lookup table:</b> States";	
         $tabletype = "tt1";
		 $luid = "1";
	  } elseif ( $group == 'category' ) {  // Retrieves donationcategories rows
	     $reporttitle = "<b>Lookup table:</b> Donation Categories (donationcategories)";
		 $tabletype = "tt2";
		 $luid = "2";
	  } elseif ( $group == 'subcategory' ) {  // Retrieves donationrestrictedsubcat rows
	     $reporttitle = "<b>Lookup table:</b> Donation Restrictes Sub-Categories (donationrestrictedsubcat)";
		 $tabletype = "tt2";
		 $luid = "3";
	  } elseif ( $group == 'contributionsource' ) {  // Retrieves contributionsource rows
	     $reporttitle = "<b>Lookup table:</b> Contribution Source (contributionsource)";
		 $tabletype = "tt2";
		 $luid = "4";
	  } else {  // Retrieves donortype rows
	     $reporttitle = "<b>Lookup table:</b> Donor Type (donortype)";
		 $tabletype = "tt2";
		 $luid = "5";
	  }
	  
?>

<h2>Lookup Table Status</h2>
<!-- display all equipment -->
<table border=1 cellpadding="6">
<?php	
	print "<tr>";
    print "  <td colspan=9 align=center bgcolor=#E7AE66>".$reporttitle."</td>"; 
    print "</tr>";
	
	if ( $tabletype == "tt1" ) {
?>

	  <tr bgcolor="#E7AE66">
		<td align="center"><b>Statecode</b></td>
		<td align="center" colspan="3"><b>State</b></td>
		<td align="center" colspan="2"><b>Edit</b></td>	
	  </tr>	
	  
  
<?php
  
    } else {
		
?>		

	  <tr bgcolor="#E7AE66">
		<td align="center"><b>Id</b></td>
		<td align="center"><b>Name</b></td>
		<td align="center"><b>Description</b></td>
		<td align="center"><b>Status</b></td>
		<td align="center"><b>Status Date</b></td>	
		<td align="center"><b>Edit</b></td>	
	  </tr>
		
<?php
    
	}

	if ( $tabletype == "tt1" ) {  
	
		# Code for your web page follows.
		try
		{
		   
		  //open the database
		  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		  
		  $sql="SELECT * FROM states ORDER BY statecode;";
		  
		  $result = $db->query($sql);
		  foreach($result as $row) {
			print "<tr>";
			print "  <td><b>".$row['statecode']."</b></td>";
			print "  <td colspan=3>".$row['state']."</td>";
			print "  <td colspan=2><a href='prj_lookupedit.php?type=".$luid."&id=".$row['statecode']."'>click to edit</a></td>";
			print "</tr>";
		  }
		  
		  // close the database connection
		  $db = NULL;
		}
		catch(PDOException $e)
		{
			echo 'Exception : '.$e->getMessage();
			echo "<br/>";
			$db = NULL;
		}
		
	} else {
		
	   if ( $group == 'category' ) {
		   
		    # Code for your web page follows.
			try
			{
			   
			  //open the database
			  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
			  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			  
			  $sql="SELECT * FROM donationcategories ORDER BY donationcategories;";
			  
			  $result = $db->query($sql);
			  foreach($result as $row) {
				print "<tr>";
				print "  <td><b>".$row['donationcategories_id']."</b></td>";
				print "  <td>".$row['donationcategories']."</td>";
				print "  <td>".$row['description']."</td>";
				print "  <td>".$row['active_id']."</td>";
				print "  <td>".$row['activedate']."</td>";
				print "  <td><a href='prj_lookupedit.php?type=".$luid."&id=".$row['donationcategories_id']."'>click to edit</a></td>";
				print "</tr>";
			  }
			  
			  // close the database connection
			  $db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}	   
	   
	   } elseif ( $group == 'subcategory' ) {

		    # Code for your web page follows.
			try
			{
			   
			  //open the database
			  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
			  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			  
			  $sql="SELECT * FROM donationrestrictedsubcat ORDER BY donationrestrictedsubcat;";
			  
			  $result = $db->query($sql);
			  foreach($result as $row) {
				print "<tr>";
				print "  <td><b>".$row['donationrestrictedsubcat_id']."</b></td>";
				print "  <td>".$row['donationrestrictedsubcat']."</td>";
				print "  <td>".$row['description']."</td>";
				print "  <td>".$row['active_id']."</td>";
				print "  <td>".$row['activedate']."</td>";
				print "  <td><a href='prj_lookupedit.php?type=".$luid."&id=".$row['donationrestrictedsubcat_id']."'>click to edit</a></td>";
				print "</tr>";
			  }
			  			  
			  // close the database connection
			  $db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
				   
	   } elseif ( $group == 'contributionsource' ) {

		    # Code for your web page follows.
			try
			{
			   
			  //open the database
			  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
			  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			  
			  $sql="SELECT * FROM contributionsource ORDER BY contributionsource;";
			  
			  $result = $db->query($sql);
			  foreach($result as $row) {
				print "<tr>";
				print "  <td><b>".$row['contributionsource_id']."</b></td>";
				print "  <td>".$row['contributionsource']."</td>";
				print "  <td>".$row['description']."</td>";
				print "  <td>".$row['active_id']."</td>";
				print "  <td>".$row['activedate']."</td>";
				print "  <td><a href='prj_lookupedit.php?type=".$luid."&id=".$row['contributionsource_id']."'>click to edit</a></td>";
				print "</tr>";
			  }
			  			  
			  // close the database connection
			  $db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
			
	   } else {

		    # Code for your web page follows.
			try
			{
			   
			  //open the database
			  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
			  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			  
			  $sql="SELECT * FROM donortype ORDER BY donortype;";
			  
			  $result = $db->query($sql);
			  foreach($result as $row) {
				print "<tr>";
				print "  <td><b>".$row['donortype_id']."</b></td>";
				print "  <td>".$row['donortype']."</td>";
				print "  <td>".$row['description']."</td>";
				print "  <td>".$row['active_id']."</td>";
				print "  <td>".$row['activedate']."</td>";
				print "  <td><a href='prj_lookupedit.php?type=".$luid."&id=".$row['donortype_id']."'>click to edit</a></td>";
				print "</tr>";
			  }
			  
			  // close the database connection
			  $db = NULL;
			}
			catch(PDOException $e)
			{
				echo 'Exception : '.$e->getMessage();
				echo "<br/>";
				$db = NULL;
			}
	   }	
		
    }	

    print "<tr>";
	print "  <td colspan=6 align=center><a href='prj_lookupadd.php?id=".$luid."'>click to add a new record</a></td>";
	print "</tr>";	
	
	print "</table><br />";
	
}	
	
require('prj_footer.php');
?>
