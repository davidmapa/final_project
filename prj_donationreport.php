<!doctype html>
<?php
require('prj_functions.php');
require('prj_values.php');
html_head("prj donation report");
require('prj_header.php');
require('prj_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{

	# Code for your web page follows.
	try
	{
	   
	  //open the database
	  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
	  $sql="SELECT CONCAT(YEAR(CURDATE()),'-01-01') AS 'begindate', CURDATE() AS 'enddate';";
	  
	  $result = $db->query($sql);
	  foreach($result as $row) {
		$begindate = $row['begindate'];
		$enddate = $row['enddate'];
	  }
	  
	  // close the database connection
	  $db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
    }
?>

	
	<h2>Donation Report Date Range and Filter</h2>
	<form action="prj_donationreport.php" method="post">
		<table border="0" cellpadding="10">
		  <tr>
		    <td align="left" colspan="2">Enter the date range to retrieve the donation records for a specific donor within the range. <br /> 
			   The default begin date is January 1 of the current year and the end date is the current date. <br /> 
			   The default dates can be change.</td>		
		  </tr>
		  <tr bgcolor="#E7AE66">
			<td width="300" align="center"><b>Begin Date</b></td>
			<td width="300" align="center"><b>End Date</b></td>			
		  </tr>
		  <tr>
			<td align="center"><input type="date" name="begindate" size="10" maxlength="10" value="<?php echo $begindate;?>"></td>
			<td align="center"><input type="date" name="enddate" size="10" maxlength="10" value="<?php echo $enddate;?>"></td>			
		  </tr>	
		  <tr>
		    <td align="center" bgcolor="#E7AE66"><b>Select Donator</b></td>
			<td align="left">
				<select name="donor">		 
					<?php
					  // Replace text field with a select pull down menu.
					  try
					  {
						//open the database
						$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
						$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

						//display all types in the contributionsource table
						$result = $db->query('SELECT donor_id,
                                                CASE donortype_id
                                                  WHEN 1 THEN companydonorname
												  WHEN 2 THEN companydonorname
												  ELSE CONCAT(firstname," ",lastname)
												END AS enityname
						                      FROM donor 
											  WHERE active_id = 1 
											  ORDER BY enityname');
						foreach($result as $row)
						{
						  print "<option value=".$row['donor_id'].">".$row['enityname']."</option>";
						}

						// close the database connection
						$db = NULL;
					  }

					  catch(PDOException $e)
					  {
						echo 'Exception : '.$e->getMessage();
						echo "<br/>";
						$db = NULL;
					  }
					?>
				</select>
			</td>
		  </tr>
		  <tr>
			<td colspan="2" align="center"><input type="submit" name="submit" value="Retrieve Records"></td>
		  </tr>
		</table>
	</form><br />	
<?php
} else {
	# Process the information from the form displayed
	$begindate = $_POST['begindate'];
	$enddate = $_POST['enddate'];
	//$group = $_POST['group1'];
	$donor = $_POST['donor']; 

	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  
		$sql="SELECT donortype_id, 
	          CASE 
			    WHEN LENGTH(TRIM(companydonorname)) = 0 THEN CONCAT(firstname,' ',lastname)
				ELSE TRIM(companydonorname)
			  END AS 'donorname',
			  addressline1, 
	          addressline2, 
			  addressline3, 
			  city, 
			  statecode, 
			  zipcode,
              CASE
                WHEN LENGTH(TRIM(phone2)) > 0 THEN CONCAT(phone1,' or ',phone2)		
                ELSE TRIM(phone1)
			  END AS 'phone',				
			  CASE
                WHEN LENGTH(TRIM(email2)) > 0 THEN CONCAT(email1,' or ',email2)		
                ELSE TRIM(email1)
			  END AS 'email'			
			FROM donor
			WHERE donor_id = '$donor';";
			
		$result = $db->query($sql);
		foreach($result as $row) {
			$donorname = $row['donorname'];
			$addressl1 = $row['addressline1'];
			$addressl2 = $row['addressline2'];
			$addressl3 = $row['addressline3'];
			$city = $row['city'];
			$state = $row['statecode'];
			$zipcode = $row['zipcode'];
			$phone = $row['phone'];
			$email = $row['email'];
		}	  
		
		// close the database connection
		$db = NULL;
	}
	catch(PDOException $e)
	{
		echo 'Exception : '.$e->getMessage();
		echo "<br/>";
		$db = NULL;
	}

	// Build address - deal with empty address line fields
	$addressl3 = trim($addressl3);
	$addressl2 = trim($addressl2);
	$addressl1 = trim($addressl1);
	if ( empty($addressl3) ) {
		if ( empty($addressl2) ) {
			$address = $addressl1;
		} else {
			$address = $addressl1."<br />".$addressl2;
		}  
	} else {
		$address = $addressl1."<br />".$addressl2."<br />".$addressl3;
	}
    
    $donorcontact = "<b>Donator:</b><br /><b>".$donorname."</b><br />".$address."<br />".$city.", ".$state." ".$zipcode."<br /><br /><b>Phone:</b> ".$phone."<br /><b>Email</b>: ".$email;	
	
	// Build Report Date line
	$reportdates = "<b>Reporting period:</b> ".$begindate." <b>through</b> ".$enddate;
	
	// Build WAMMP Contact
	$wammp = "<b>WAMMP<br /> West Africa Mission Mobilization Project </b><br />P.O. Box 850<br />Rough And Ready, CA 95975<br /><br /><b>Phone:</b>605-786-7723<br /><b>Email:</b>David.Mapa_a2z@yahoo.com";

?>


<h2>Donation Report</h2>
<!-- display all equipment -->
<table border=0 cellpadding=10>
<?php  
   print "<tr>";
   print "  <td colspan=6 align=center bgcolor=#E7AE66>".$reportdates."</td>"; 
   print "</tr>";
   print "<tr>";
   print "  <td colspan=3 align=left>".$donorcontact."</td>"; 
   print "  <td colspan=3 align=left>".$wammp."</td>"; 
   print "</tr>";   
?>  
  <tr bgcolor="#E7AE66">
    <td align="center"><b>Donation Id</b></td>
	<td align="center"><b>Donation Category</b></td>
	<td align="center"><b>Donation Sub-category</b></td>
	<td align="center"><b>Donation Date</b></td>	
	<td align="center"><b>Contribution Amount</b></td>
	<td align="center"><b>Inkind Amount</b></td>
  </tr>

<?php
  
	try
	{
	  
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		  
		$sql="SELECT dn.donation_id as 'donationid', 
				  trim(CONCAT(dr.firstname,' ', dr.lastname)) as 'donorname', 
				  trim(dr.companydonorname) as 'companyname',
				  trim(dc.donationcategories) as 'donationcategories',
				  trim(dsc.donationrestrictedsubcat) as 'donationsubcategories',
				  dn.donationdate as 'donationdate',
				  dn.contributionamount as 'contributionamount',
				  dn.inkind_actualvalue as 'inkind_actualvalue'
				FROM donation as dn
				  LEFT JOIN donor as dr ON dn.donor_id = dr.donor_id
				  LEFT JOIN donationcategories as dc ON dn.donationcategories_id = dc.donationcategories_id
				  LEFT JOIN donationrestrictedsubcat as dsc ON dn.donationrestrictedsubcat_id = dsc.donationrestrictedsubcat_id
				WHERE dn.donationdate BETWEEN '$begindate' AND '$enddate'
				  AND dn.donor_id = '$donor'
				ORDER BY dr.donor_id, donationdate;";
		
  
	    $result = $db->query($sql);
	    foreach($result as $row) {
			print "<tr>";
			print "  <td><b>".$row['donationid']."</b></td>";
			print "  <td>".$row['donationcategories']."</td>";
			print "  <td>".$row['donationsubcategories']."</td>";
			print "  <td>".$row['donationdate']."</td>";	
			print "  <td align=right>".number_format($row['contributionamount'],2)."</td>";	
			print "  <td align=right>".number_format($row['inkind_actualvalue'],2)."</td>";
			print "</tr>";
	    }
		
		if ( $group == 'all' ) { 
  
			$sql2="SELECT SUM(contributionamount) as 'sumcontributionamount',
                   SUM(inkind_actualvalue) as 'suminkindactualvalue'
				   FROM donation
				   WHERE donationdate BETWEEN '$begindate' AND '$enddate';";
			
		} else {
			
			$sql2="SELECT SUM(contributionamount) as 'sumcontributionamount',
                   SUM(inkind_actualvalue) as 'suminkindactualvalue'
				   FROM donation
				   WHERE donationdate BETWEEN '$begindate' AND '$enddate'
				   AND donor_id = '$donor';";			
		
		}
		 
		$result2 = $db->query($sql2);
		foreach($result2 as $row) {
			print "<tr>";
			print "  <td colspan=4 align=center bgcolor=#E7AE66><b>Contribution Total</b></td>";
			print "  <td align=right><b>".number_format($row['sumcontributionamount'],2)."</b></td>";
			print "  <td align=right><b>".number_format($row['suminkindactualvalue'],2)."</b></td>";
			//print "  <td></td>";
			//print "  <td></td>";
			print "</tr>";
		}	

		print "</table><br />";

	  // close the database connection
	  $db = NULL;
	}
	catch(PDOException $e)
	{
	  echo 'Exception : '.$e->getMessage();
	  echo "<br/>";
	  $db = NULL;
	}
}	
	
require('prj_footer.php');
?>
